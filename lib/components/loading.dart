import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loading extends StatelessWidget {
  final double size;

  Color color = Color.fromARGB(255, 27, 130, 210);

  Loading({this.size: 16, this.color});
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return
        // height: double.infinity,
        Center(
            child: SpinKitWave(
      size: this.size,
      color: Color.fromARGB(255, 27, 130, 210),
    ));
  }
}
