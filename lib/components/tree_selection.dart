import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class TreeNode {
  String code;
  bool hasChildren;
  List<TreeNode> children;
  bool expanded = false;
  String title;
  String status;
  TreeNode(this.code, this.title,
      {this.hasChildren = true,
      this.children,
      this.expanded = false,
      this.status = 'created'});
}

typedef SelectionCallback = void Function(TreeNode node);

typedef AsyncTreeNodeLoader = Future<List<TreeNode>> Function(TreeNode parent);

// List<TreeNode> demo = List();

// final TreeNode t1 = TreeNode('0001', '安全管理');
// final TreeNode t2 = TreeNode('0002', '现场管理');

// final TreeNode t1 = TreeNode(
//     '0001',
//     '安全管理',
//     true,
//     <TreeNode>[
//       TreeNode('000101', '安全管理1', false, [], false),
//       TreeNode('000102', '安全管理2', false, [], false),
//       TreeNode('000103', '安全管理3', false, [], false),
//       TreeNode('000104', '安全管理4', false, [], false),
//       TreeNode('000105', '安全管理5', false, [], false),
//       TreeNode('000106', '其他', false, [], false),
//     ],
//     false);

// final TreeNode t2 = TreeNode(
//     '0002',
//     '现场管理',
//     true,
//     <TreeNode>[
//       TreeNode('000101', '现场管理1', false, [], false),
//       TreeNode('000102', '现场管理2', false, [], false),
//       TreeNode('000103', '现场管理3', false, [], false),
//       TreeNode('000104', '现场管理4', false, [], false),
//       TreeNode('000105', '现场管理5', false, [], false),
//       TreeNode('000106', '现场管理6', false, [], false),
//     ],
//     false);
class TreeSelectionWidget extends StatefulWidget {
  List<TreeNode> children;

  SelectionCallback callback;

  TreeSelectionWidget(this.children, this.callback);

  @override
  State<StatefulWidget> createState() => _TreeSelectionState();
}

class _TreeSelectionState extends State<TreeSelectionWidget> {
  buildPanel() {
    return widget.children.map((f) {
      Widget body = buildPanelBody(f.children);
      return ExpansionPanel(
        isExpanded: f.expanded,
        headerBuilder: (BuildContext context, bool isExpanded) {
          return Container(
            // color: Colors.blue,
            child: Text(f.title),
            padding: EdgeInsets.all(16),
          );
        },
        body: buildPanelBody(f.children)
      );
    }).toList();
  }
  buildPanelBody(List<TreeNode> children) {
    return Container(
      // width: MediaQuery.of(context).size.width,
      color: Color.fromARGB(255, 248, 248, 255),

      // padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        // crossAxisSize:
        crossAxisAlignment: CrossAxisAlignment.start,
        // mainAxisSize: MainAxisSize.max,
        children: ListTile.divideTiles(
                tiles: children.map((c) {
                  return InkWell(
                    child: ListTile(
                      contentPadding: EdgeInsets.all(-16),
                      title: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 16),
                          child: Text(c.title)),
                    ),
                    onTap: () {
                      widget.callback(c);
                      Navigator.pop(context);
                    },
                  );
                  // return Container(
                  //     decoration: BoxDecoration(
                  //       border: Border(bottom: BorderSide(
                  //         color: Colors.grey,
                  //         width: 1,
                  //       ))
                  //     ),
                  //     padding: EdgeInsets.symmetric(vertical: 16),
                  //     child: Row(children: [Text(c.title)]));
                }).toList(),
                color: Colors.grey)
            .toList(),
      ),
    );
  }
  _loadNodeChildren(node, bool expadned) {
    setState(() {
          node.expanded = expadned;
        });
  }
  Widget build(BuildContext context) {
    return Container(
      child: ExpansionPanelList(
         expansionCallback: (int index, bool expanded) {
            print('expaded $index, $expanded');

            TreeNode node = widget.children[index];
            // String status = node.status;
            _loadNodeChildren(node, !expanded);
            // if (node.hasChildren && node.status != 'done') {

            // }
            // demo[index].expanded = !expanded;
            // if (demo[index].children == null || demo[index].children.length <=0) {
            //   widget.future.
            // }

            // print('$status');
            // setState(() {
            //   node.expanded = !expanded;
            //   node.status = status;
            // });
          },
          
          children: buildPanel()),
    );
  }
}

class AsyncTreeSeletctionWidget extends StatefulWidget {
  // Future<List<TreeNode>> future;

  List<TreeNode> children;
  // Function f;
  AsyncTreeNodeLoader asyncLoader;

  SelectionCallback callback;

  AsyncTreeSeletctionWidget(this.children, this.asyncLoader, this.callback);
  @override
  State<StatefulWidget> createState() => _AsyncTreeSeletectionState();
}

class _AsyncTreeSeletectionState extends State<AsyncTreeSeletctionWidget> {
  Widget _buildNodeWidget(BuildContext context, TreeNode node) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(16),
      child: Text(node.title),
    );
  }

  @override
  initState() {
    super.initState();
    // demo.clear();
    // demo.add(t1);
    // demo.add(t2);
    TreeNode treeNode = widget.children[0];
    if (treeNode.status != 'done') {
      _loadNodeChildren(treeNode, true);
    }
  }

  void _loadNodeChildren(TreeNode node, bool expand) {
    String status = node.status;
    print('Got future');
    print(node.status);
    print(node.status != 'done');
    if (node.status != 'done') {
      print('加载下级 ${node.code}');
      status = 'loading';
      Future<List<TreeNode>> future = widget.asyncLoader(node);

      future.then((List<TreeNode> treeNodes) {
        if (mounted) {
          setState(() {
            node.children = treeNodes;
            node.status = 'done';
            if (expand) {
              for (TreeNode n in widget.children) {
                if (n.code != node.code) {
                  n.expanded = false;
                }
              }
            }
          });
        }
      });
    }
    setState(() {
      node.status = status;
      node.expanded = expand;
    });
  }

  @override
  Widget build(BuildContext context) {
    ListView v;
    List<ExpansionPanel> panels = widget.children.map((f) {
      print('render ${f.code}, staus: ${f.status}');
      Widget body = Container();
      if (f.expanded) {
        switch (f.status) {
          case 'created':
            {
              body = Container();
              break;
            }
          case 'loading':
            {
              body = Container(
                color: Color.fromARGB(255, 241, 241, 241),
                padding: EdgeInsets.all(16),
                child: SpinKitWave(
                  color: Theme.of(context).primaryColor,
                  size: 18,
                ),
              );
              break;
            }
          case 'done':
            {
              List<TreeNode> children = f.children;
              body = Container(
                // width: MediaQuery.of(context).size.width,
                color: Color.fromARGB(255, 248, 248, 255),

                // padding: EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  // crossAxisSize:
                  crossAxisAlignment: CrossAxisAlignment.start,
                  // mainAxisSize: MainAxisSize.max,
                  children: ListTile.divideTiles(
                          tiles: children.map((c) {
                            return InkWell(
                              child: ListTile(
                                contentPadding: EdgeInsets.all(-16),
                                title: Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 16),
                                    child: Text(c.title)),
                              ),
                              onTap: () {
                                widget.callback(c);
                                Navigator.pop(context);
                              },
                            );
                            // return Container(
                            //     decoration: BoxDecoration(
                            //       border: Border(bottom: BorderSide(
                            //         color: Colors.grey,
                            //         width: 1,
                            //       ))
                            //     ),
                            //     padding: EdgeInsets.symmetric(vertical: 16),
                            //     child: Row(children: [Text(c.title)]));
                          }).toList(),
                          color: Colors.grey)
                      .toList(),
                ),
              );
              break;
            }
        }
      }
      return ExpansionPanel(
        isExpanded: f.expanded,
        headerBuilder: (BuildContext context, bool isExpanded) {
          return Container(
            // color: Colors.blue,
            child: Text(f.title),
            padding: EdgeInsets.all(16),
          );
        },
        body: body,
      );
    }).toList();
    return Container(
      child: ExpansionPanelList(
          expansionCallback: (int index, bool expanded) {
            print('expaded $index, $expanded');

            TreeNode node = widget.children[index];
            // String status = node.status;
            _loadNodeChildren(node, !expanded);
            // if (node.hasChildren && node.status != 'done') {

            // }
            // demo[index].expanded = !expanded;
            // if (demo[index].children == null || demo[index].children.length <=0) {
            //   widget.future.
            // }

            // print('$status');
            // setState(() {
            //   node.expanded = !expanded;
            //   node.status = status;
            // });
          },
          children: panels),
    );
  }
}
