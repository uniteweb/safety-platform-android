import 'package:flutter/material.dart';

class DangerForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _DangerFormState();
  }
}

class _DangerFormState extends State<DangerForm> {
  List<String> dangerLevel = [
    '重大事故隐患',
    '一般事故隐患',
  ];
  Widget singleListDialog(BuildContext context,
      {String title, List<String> child, Function(String) fn}) {
    Widget titleWidget;
    if (title != null) {
      titleWidget = Container(
        width: MediaQuery.of(context).size.width,
        child: Center(child: Text('隐患级别')),
      );
    }
    return SimpleDialog(
        title: titleWidget,
        contentPadding: EdgeInsets.all(0),
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: dangerLevel.map((f) {
              return GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                  if (fn != null) {
                    fn(f);
                  }
                },
                child: Container(
                  // height: 50,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        width: 1,
                        color: Color.fromARGB(255, 241, 241, 241),
                      ),
                    ),
                  ),
                  child: Text(f),
                ),
              );
              // return Text(f);
            }).toList(),
            // shrinkWrap: true,
          ),
        ]);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromARGB(255, 245, 245, 249),

      width: MediaQuery.of(context).size.width,
      // padding: EdgeInsets.all(16),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border(
                bottom: BorderSide(
                  width: 1,
                  color: Color.fromARGB(255, 241, 241, 241),
                ),
              ),
            ),
            // color: Colors.red,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('隐患类别'),
                GestureDetector(
                  child: Text('请选择隐患类别'),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border(
                bottom: BorderSide(
                  width: 1,
                  color: Color.fromARGB(255, 241, 241, 241),
                ),
              ),
            ),
            // color: Colors.red,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('整改日期'),
                GestureDetector(
                  child: Text('请选择隐患类别'),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(16),

            decoration: BoxDecoration(
              color: Colors.white,
              border: Border(
                bottom: BorderSide(
                  width: 1,
                  color: Color.fromARGB(255, 241, 241, 241),
                ),
              ),
            ),
            // color: Colors.red,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('风险单元'),
                GestureDetector(
                  child: Text('请选择风险单元'),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(16),

            decoration: BoxDecoration(
              color: Colors.white,
              border: Border(
                bottom: BorderSide(
                  width: 1,
                  color: Color.fromARGB(255, 241, 241, 241),
                ),
              ),
            ),
            // color: Colors.red,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('隐患级别'),
                GestureDetector(
                  child: Text('请选择隐患级别'),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(16),
            color: Colors.white,
            child: TextField(
              maxLines: 4,
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(0),
                  border: InputBorder.none,
                  // hintText: searchFieldLabel,

                  hintText: '请填写具体情况及整改方案',
                  hintStyle: TextStyle(
                    fontSize: 14,
                  )),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 40,
            margin: EdgeInsets.only(top: 16),
            padding: EdgeInsets.fromLTRB(32, 0, 32, 0),
            child: RaisedButton(
              color: Colors.red,
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return singleListDialog(context,
                          title: null, fn: (s) => print("选择了$s"));
                    });
              },
              textColor: Colors.white,
              child: Text('隐患提交'),
            ),
          )
        ],
      ),
    );
  }
}
