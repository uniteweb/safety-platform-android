import 'package:flutter/material.dart';
import 'package:safety_platform/models/attachment.dart';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:safety_platform/pages/gallery.dart';

class AttachmentPreviewGallery extends StatefulWidget {
  final List<Attachment> attachments;

  final bool collapse;

  final String collapseLabel;

  // final

  AttachmentPreviewGallery(
      {@required this.attachments, this.collapseLabel, this.collapse: true});
  State<StatefulWidget> createState() => _AttachmentPreviewState();
}

class _AttachmentPreviewState extends State<AttachmentPreviewGallery> {
  bool collapse = true;
  Widget _attachmentItems() {
    List<PhotoViewGalleryPageOptions> pageOptions =
        widget.attachments.map((attach) {
      return PhotoViewGalleryPageOptions(
          heroTag: 'aaiisisisis',
          initialScale: 1.00,
          maxScale: 1.50,
          imageProvider: CachedNetworkImageProvider(
            attach.url,
          ));
    }).toList();
    return PhotoViewGallery(
      pageOptions: pageOptions,
    );
  }

  Widget _collapseRow() {
    String label = widget.collapseLabel != null
        ? '${widget.collapseLabel} 共${widget.attachments.length} 张'
        : '';
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 2),
          child: FlatButton.icon(
            onPressed: () {
              List<String> urls = widget.attachments.map((attach) {
                return attach.url;
              }).toList();
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return GalleryPage(urls);
              }));
            },
            icon: Icon(
              Icons.image,
              color: Theme.of(context).primaryColor,
            ),
            label: Text(
              label,
              style: TextStyle(
                color: Colors.blue,
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget build(BuildContext buildContext) {
    Widget itemWidget;
    if ((widget.attachments == null || widget.attachments.length <= 0)) {
      return Container();
    }
    if (collapse) {
      itemWidget = _collapseRow();
    } else {
      itemWidget = _attachmentItems();
    }
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          itemWidget,
          // _collapseRow(),
        ],
      ),
    );
  }
}

class AttachmentGallery extends StatelessWidget {
  final List<Attachment> attachments;

  final String uploadLabel;
  final VoidCallback onTakePhoto;
  final Color backgoundColor;
  AttachmentGallery({
    @required this.attachments,
    this.uploadLabel,
    this.backgoundColor,
    this.onTakePhoto,
  });

  // AttachmentGallery(this.attachments);

  Widget _uploadRow(BuildContext context) {
    if (uploadLabel != null) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 2),
            child: FlatButton.icon(
              onPressed: () {
                if (onTakePhoto != null) {
                  onTakePhoto();
                }
              },
              icon: Icon(
                Icons.camera_enhance,
                color: Theme.of(context).primaryColor,
              ),
              label: Text(this.uploadLabel),
            ),
          )
        ],
      );
    }
    return Container();
  }

  Widget _galleryItems() {
    if (this.attachments != null) {
      List<Widget> children = this.attachments.map((i) {
        Widget widget;
        if (i.uploadStatus != 'done') {
          widget = SizedBox(
            width: 80,
            height: 80,
            child: Stack(
              children: <Widget>[
                Image.file(
                  File(i.localPath),
                  width: 80,
                  height: 80,
                  fit: BoxFit.fill,
                ),
                new Opacity(
                  opacity: 0.5, //不透明度
                  child: new Container(
                      width: 80.0, height: 80.0, color: Colors.black),
                ),
                Center(
                  child: CircularProgressIndicator(
                    // semanticsLabel: '上传中',
                    // semanticsValue: '10%',
                    backgroundColor: Colors.white,
                    // value: 0.00,
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                    strokeWidth: 2,
                  ),
                )
                // CachedNetworkImage(
                //   imageUrl: i.localPath,
                // )
              ],
            ),
          );
        } else {
          if (i.localPath != null) {
            widget = Image.file(File(i.localPath),
                width: 80, height: 80, fit: BoxFit.fill);
          } else {
            widget = CachedNetworkImage(
              imageUrl: i.url,
              width: 80,
              height: 80,
              fit: BoxFit.fill,
              fadeInDuration: Duration(milliseconds: 200),
              fadeOutDuration: Duration(milliseconds: 200),
            );
          }
        }
        return Container(
          child: widget,
          margin: EdgeInsets.only(right: 8),
        );
      }).toList();
      if (children != null && children.length > 0) {
        return Container(
          margin: EdgeInsets.fromLTRB(16, 8, 16, 8),
          child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Container(
              child: Row(
                children: children,
                mainAxisAlignment: MainAxisAlignment.start,
              ),
            ),
          ),
        );
      } else {
        return Container();
      }
    }
  }

  Widget build(BuildContext context) {
    return Container(
      color: backgoundColor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _galleryItems(),
          _uploadRow(context),
        ],
      ),
    );
  }
}
