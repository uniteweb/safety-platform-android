import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:safety_platform/pages/services.dart';
import 'package:safety_platform/models/advertisement.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'dart:io';

class CarouselSliderWidget extends StatefulWidget {
  final double height;

  final Map<String, String> extraMap;

  final bool autoPlay;
  const CarouselSliderWidget(
      {this.height: 120.0, this.extraMap, this.autoPlay: true});
  State<StatefulWidget> createState() => _CarouseSliderState();
}

class _CarouseSliderState extends State<CarouselSliderWidget> {
  List<Widget> items = List();

  @override
  void initState() {
    super.initState();

    fetchAdvFormCache().then((List<Advertisement> advs) {
      if (advs == null) {
        Widget widget = Container(
            width: MediaQuery.of(context).size.width,
            margin: new EdgeInsets.symmetric(horizontal: 5.0),
            // decoration: new BoxDecoration(color: Colors.amber),
            child: new Image.asset(
              'assets/bannerx.png',
              fit: BoxFit.fill,
            ));
        if (mounted) {
          setState(() {
            items.clear();
            items.add(widget);
            // items.add(widget);
          });
        }
      }
    });
    String platfrom = 'WEB';

    if (Platform.isAndroid) {
      platfrom = 'Android';
    } else if (Platform.isIOS) {
      platfrom = 'IOS';
    }
    fetchAdvFromRemote(position: 'HOME', platform: platfrom)
        .then((List<Advertisement> advList) {
      if (advList != null && mounted) {
        items = advList.map((adv) {
          return Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.symmetric(horizontal: 5.0),
              // decoration: new BoxDecoration(color: Colors.amber),
              child: CachedNetworkImage(
                fit: BoxFit.fill,
                imageUrl: adv.content.imageUrl,
                placeholder: SpinKitCircle(
                  size: 24,
                  color: Theme.of(context).primaryColor,
                ),
                errorWidget: Icon(Icons.error),
              ));
        }).toList();
        setState(() {
          items = items;
        });
      }
    });
  }

  Widget build(BuildContext context) {
    print('-------> ${items.length}');
    if (items == null || items.length == 0) {
      items.addAll([1, 2, 3].map((i) {
        Container(
            width: MediaQuery.of(context).size.width,
            margin: new EdgeInsets.symmetric(horizontal: 5.0),
            decoration: new BoxDecoration(color: Colors.amber),
            child: new Image.asset(
              'assets/banner_${i.toString()}.jpg',
              fit: BoxFit.fill,
            ));
      }));
    }

    return CarouselSlider(
      items: items,
      height: widget.height,
      autoPlay: widget.autoPlay,
    );
    // return CarouselSlider(
    //                     items: []
    //                     height: 120.0,
    //                     autoPlay: true);
  }
}
