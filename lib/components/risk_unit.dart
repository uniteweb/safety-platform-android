import 'package:flutter/material.dart';
import 'package:safety_platform/models/risk_unit.dart';
import 'package:safety_platform/models/risk.dart';
import 'package:safety_platform/utils.dart';

TextStyle whiteText = TextStyle(color: Colors.white);

typedef OnRiskTapCallback  = Function(Risk risk);
class RiskUnitWidget extends StatelessWidget {
  final RiskUnit riskUnit;

  final OnRiskTapCallback onPress;


  const RiskUnitWidget(this.riskUnit, {this.onPress});

  @override
  Widget build(BuildContext context) {
    return _buildItem(context);
  }

  Widget _statusText(String title, String value, double width) {
    if (value == null || value.trim().isEmpty) {
      value = '暂无数据';
    }
    return Container(
      width: width,
      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 8),
            child: Text(
              title,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
          Text(
            value?? '暂无数据',
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: whiteText,
          ),
        ],
      ),
    );
  }

  List<Widget> _buildRiskItem(List<Risk> tasks, BuildContext context) {
    // double statusWidth = 180;

    double statusWidth =
        (MediaQuery.of(context).size.width - 16 * 2 - 8 * 2 - 10) / 3;

    ///MediaQuery.of(context).size.width - 16 - 8 / 3;
    return tasks.map((task) {
      Risk f = task;
      return GestureDetector(
          onTap: () {
            print("跳转到任务: $f");
            if (onPress != null) {
              onPress(f);
              // Navigator.of(context).push(
              //   MaterialPageRoute(builder: (BuildContext context) {
              //     // return RiskUnitPage(task: task);
              //   }),
              // );
            }
          },
          child: Container(
            margin: EdgeInsets.fromLTRB(8, 8, 8, 8),
            // padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5.0),
                  topRight: Radius.circular(5.0)),
              border: new Border.all(width: 1.0, color: Colors.white),
              color: Colors.white,
            ),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                            width: MediaQuery.of(context).size.width -
                                16 * 2 -
                                1 * 2,
                            padding: EdgeInsets.fromLTRB(8, 8, 4, 4),
                            // margin: EdgeInsets.fromLTRB(0, 0, 0, 4),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  '风险点名称',
                                  style: TextStyle(fontWeight: FontWeight.w700),
                                ),
                                // _buildStatusText(task.status),
                              ],
                            )),
                        Container(
                          padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                          width: MediaQuery.of(context).size.width -
                              16 * 2 -
                              1 * 2,
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 4),
                          child: Text('${f.name}',
                              overflow: TextOverflow.ellipsis,
                              maxLines: 6,
                              style: TextStyle(color: Colors.grey)),
                        )
                      ],
                    )
                  ],
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(5.0),
                        bottomLeft: Radius.circular(5.0)),
                    border: new Border.all(width: 1.0, color: Colors.blue),
                    color: Colors.blue,
                  ),
                  margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
                  padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          _statusText('风险点编号', '${f.code}', statusWidth),
                          _statusText(
                              '风险等级', riskLevelTransform(f.level), statusWidth),
                          _statusText(
                              '风险因素', '${f.factors.length} 个', statusWidth),
                          // Column(
                          //   children: <Widget>[
                          //     Padding(
                          //       padding: EdgeInsets.fromLTRB(0, 0, 0, 8),
                          //       child: Text(
                          //         '风险等级',
                          //         style: whiteText,
                          //       ),
                          //     ),
                          //     Text(
                          //       '${f.level}',
                          //       style: whiteText,
                          //     ),
                          //   ],
                          // ),
                          // Column(
                          //   children: <Widget>[
                          //     Padding(
                          //       padding: EdgeInsets.fromLTRB(0, 0, 0, 8),
                          //       child: Text(
                          //         '风险类型',
                          //         style: whiteText,
                          //       ),
                          //     ),
                          //     Text(
                          //       '${f.classification.join(",")}',
                          //       style: whiteText,
                          //     ),
                          //   ],
                          // ),
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ));
    }).toList();
  }

  Widget _buildItem(context) {
    RiskUnit content = this.riskUnit;

    var children = <Widget>[
      Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(8),
        color: Color.fromARGB(255, 27, 130, 210),
        child: Text('单元:${content.unitName}',
            style: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.w600,
            )),
      ),
      Container(
        padding: EdgeInsets.fromLTRB(0, 4, 0, 4),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
              bottom: BorderSide(
                width: 1.0,
                color: Color.fromARGB(255, 241, 241, 241),
                style: BorderStyle.solid,
              ),
            )),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                padding: EdgeInsets.all(8.0),
                width: MediaQuery.of(context).size.width,
                child: Text.rich(
                  TextSpan(text: '编号:', children: <TextSpan>[
                    TextSpan(
                        text: ' ${content.unitCode}',
                        style: TextStyle(
                          color: Colors.grey,
                        ))
                  ]),
                ),
                decoration: BoxDecoration(
                  border: Border(
                      right: BorderSide(
                    width: 1.0,
                    color: Color.fromARGB(255, 241, 241, 241),
                    style: BorderStyle.solid,
                  )),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                padding: EdgeInsets.all(8.0),
                width: MediaQuery.of(context).size.width,
                child: Text.rich(
                  TextSpan(text: '类别:', children: <TextSpan>[
                    TextSpan(
                        text: ' ${content.category.title}',
                        style: TextStyle(
                          color: Colors.grey,
                        ))
                  ]),
                ),
              ),
            ),
          ],
        ),
      ),
      // Container(
      //     margin: EdgeInsets.all(8.0),
      //     width: MediaQuery.of(context).size.width,
      //     color: Colors.white,
      //     child: ListView(
      //       shrinkWrap: true,
      //       children: _buildRiskItem(content, context),
      //     ))
    ];
    if (content.risks != null) {
      List<Widget> riskWidgets = _buildRiskItem(content.risks, context);
      children.addAll(riskWidgets);
    }
    
    return SingleChildScrollView(child:Container(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 16.0),
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: children,
        // Container(
        //   color:Color.fromARGB(255, 27, 130, 210) ,
        //   )
      ),
    ),);
  }
}
