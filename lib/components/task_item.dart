import 'package:flutter/material.dart';
import 'package:safety_platform/models/task_item.dart';
import 'package:safety_platform/theme.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:cached_network_image/cached_network_image.dart';
class TaskItemWidget extends StatelessWidget {

  final TaskItem item;

  const TaskItemWidget(this.item);

    Widget _buildAttachment(BuildContext context, TaskItem item) {
    if (item.attachments == null || item.attachments.length <= 0) {
      return Container();
    }
    List<Widget> children = item.attachments.map((attach) {
      print(attach.url);
      
      return  Container(
          margin: EdgeInsets.only(right: 8),
          width: 80,
          height: 80,
          child: CachedNetworkImage(
            imageUrl:attach.url,
             fit: BoxFit.fill,
             placeholder: SpinKitCircle(size: 24, color:Theme.of(context).primaryColor),
             errorWidget: Icon(Icons.no_sim),
            ),
           
            
          );
    }).toList();
    
    return SingleChildScrollView( 
      scrollDirection: Axis.horizontal,
      child: Container(
      margin: EdgeInsets.only(top: 8),
      child: Row(
        children: children,
      ),
    ),);
  }

  Widget build(BuildContext context) {
    
      Color color = Colors.grey; //.of(context).primaryColor;
      String result = item.result;
      if (result == null) {
        result = '未检查';
      } else if (result == item.rightAnswer) {
        color = rightAnswerColor;
      } else {
        color = negtiveAnswerColor;
      }
      return DecoratedBox(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Padding(
                      child: Container(
                        decoration: BoxDecoration(
                             color: color,
                          borderRadius: BorderRadius.circular(1),
                        ),
                        child: Text(result,
                            style: TextStyle(
                              color: Colors.white,
                            )),
                     
                        padding: EdgeInsets.all(4),
                      ),
                      padding: EdgeInsets.only(right: 8)),
                  Text(
                    item.content,
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      child: Text('备注:', style: secondTextStyle),
                      padding: EdgeInsets.only(bottom: 4),
                    ),
                    Text(item.remark ?? '暂无备注', style: secondTextStyle,
                        overflow: TextOverflow.clip,
                        softWrap:true,
                    ),
                  ],
                ),
              ),
              _buildAttachment(context,item),
            ],
          ),
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            bottom: BorderSide(
              color: borderColor,
              width: 0.5,
            ),
          ),
        ),
      );
    }
  
}