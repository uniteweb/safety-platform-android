import 'package:flutter/material.dart';
import 'package:safety_platform/models/risk.dart';
import 'package:safety_platform/theme.dart' show lowColor,highColor,normalColor;

class RiskDetailWidget extends StatelessWidget {
  final Risk risk;

  const RiskDetailWidget(this.risk);
  Widget _solutionItem(BuildContext context, RiskFactor factor, double width) {
    Color color = Colors.transparent;
    switch (factor.level) {
      case 'low':
        color =lowColor;
        break;
      case 'normal':
        color =normalColor;
        break;
      case 'high':
        color =highColor;
        break;
    
    }

    return Card(
      color: Colors.white,
      margin: EdgeInsets.only(bottom: 4),
      elevation: 0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              border: Border(
                  bottom: BorderSide(
                      color: Color.fromARGB(255, 241, 241, 241), width: 0.5)),
            ),
            padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                  Row(
                   children: <Widget>[ Container(
                    decoration: BoxDecoration(
                        color: color,
                        borderRadius: BorderRadius.all(Radius.circular(2.0))),
                    // color: Colors.blue,
                    width: 8,
                    child: SizedBox(
                      width: 4.0,
                      child: Text(' '),
                    ),
                  ),
                  Padding(
                      child: Text('风险度 ${factor.grade}',
                      style:TextStyle(
                        color: Colors.black45
                      ),
                      ), padding: EdgeInsets.only(left: 8)),
                   ]),
                  Container(
                    alignment: Alignment.centerRight,
                    padding: EdgeInsets.symmetric(horizontal: 8),
                    child: DecoratedBox(
                      child: Padding(
                          padding: EdgeInsets.fromLTRB(4, 0, 4, 0),
                          child: Text('LSR',
                              style: TextStyle(color: Colors.white))),
                      decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.all(Radius.circular(2))),
                    ),
                  )
                ]),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _titleWithDesc(context, '描述', '${factor.content}'),
                _titleWithDesc(
                    context, '可能发生的事故类型', factor.accidents.join(',')),
                _titleWithDesc(context, '管控措施', factor.solutions.join(',')),
                _titleWithDesc(context, '责任单位', factor.responseUnit),
                _titleWithDesc(context,  '责任人', factor.responsePerson),
              ],
              
            ),
          ),
          // Container(padding: EdgeInsets.fromLTRB(0, 8, 8, 8),
          // child: Text('展开',style:TextStyle(
          //   color:Colors.blueAccent
          // )),
          // ),
        ],
      ),
    );
    // return Container(
    //   // padding: EdgeInsets.fromLTRB(0, 12, 0, 12),
    //   child: ListTile(
    //     // subtitle: Container(
    //     //   margin: EdgeInsets.symmetric(vertical: 6),
    //     //   // color: Colors.blue,
    //     //   child: Text(
    //     //     '一般风险',
    //     //     style: TextStyle(color: Colors.black54),
    //     //   ),
    //     // ),
    //     // subtitle: Text('风险度: ${factor.grade}'),
    //     trailing: CircleAvatar(
    //       radius: 16,
    //               backgroundColor: Color.fromARGB(255, 255, 255, 11),
    //               child:Text('${factor.grade}',
    //               style:TextStyle(
    //                 // color: Colors.black/
    //                 color: Colors.black26
    //               )
    //               ),
    //     ),
    //     title: Text(
    //       factor.content,
    //       maxLines: 3,
    //       style: TextStyle(
    //         fontSize: 15,
    //         // color: Colors.black54
    //       ),
    //       overflow: TextOverflow.ellipsis,
    //     ),
    //     contentPadding: EdgeInsets.all(0),
    //   ),
    //   width: width,
    //   decoration: BoxDecoration(
    //       // color: Colors.red,
    //             color: Colors.blue,

    //       border: Border(
    //           bottom: BorderSide(
    //     color: Color.fromARGB(255, 241, 241, 241),
    //     width: 0.5,
    //   ))),
    // );
  }

  // _responseArea() {
  //   String responsePerson = '暂无数据';

  //   String contactPhone = '暂无数据';
  //   return Row(
  //     crossAxisAlignment: CrossAxisAlignment.center,
  //     mainAxisAlignment: MainAxisAlignment.center,
  //     children: <Widget>[
  //       Expanded(
  //         child: Container(
  //           // color: Colors.red,
  //           decoration: BoxDecoration(
  //               border:
  //                   Border(right: BorderSide(width: 0.5, color: Colors.white))),
  //           child: Column(
  //             crossAxisAlignment: CrossAxisAlignment.center,
  //             mainAxisAlignment: MainAxisAlignment.center,
  //             children: <Widget>[
  //               Text(
  //                 '负责人',
  //                 style: TextStyle(
  //                     color: Colors.white, fontWeight: FontWeight.w600),
  //               ),
  //               Text(
  //                 responsePerson,
  //                 style: TextStyle(color: Colors.white),
  //               ),
  //             ],
  //           ),
  //         ),
  //       ),
  //       Expanded(
  //         child: Container(
  //           child: Column(
  //             mainAxisAlignment: MainAxisAlignment.center,
  //             crossAxisAlignment: CrossAxisAlignment.center,
  //             children: <Widget>[
  //               Text(
  //                 '联系电话',
  //                 style: TextStyle(
  //                     color: Colors.white, fontWeight: FontWeight.w600),
  //               ),
  //               Text(
  //                 contactPhone,
  //                 style: TextStyle(color: Colors.white),
  //               ),
  //             ],
  //           ),
  //         ),
  //       )
  //     ],
  //   );
  // }

  // _remarkArea() {
  //   return Padding(
  //     padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
  //     child: Column(
  //       mainAxisAlignment: MainAxisAlignment.start,
  //       crossAxisAlignment: CrossAxisAlignment.start,
  //       children: <Widget>[
  //         Padding(
  //           padding: EdgeInsets.fromLTRB(0, 0, 0, 4),
  //           child: Text(
  //             '备注: ',
  //             maxLines: 4,
  //             style: TextStyle(
  //               fontWeight: FontWeight.w700,
  //             ),
  //           ),
  //         ),
  //         Padding(
  //           padding: EdgeInsets.fromLTRB(0, 0, 0, 4),
  //           child: Text(
  //             '暂无风险点其他说明',
  //             maxLines: 4,
  //             style: TextStyle(),
  //           ),
  //         )
  //       ],
  //     ),
  //   );
  // }

  _solutionWidget(BuildContext buildContext) {
    // print("风险管控措施${this.risk.solutions}");
    double width = MediaQuery.of(buildContext).size.width - 8 * 2;
    // Risk risk = risk;
    if (risk.factors != null && risk.factors.length > 0) {
      List<Widget> widges = risk.factors.map<Widget>((s) {
        return Container(
          // padding: EdgeInsets.fromLTRB(8, 0, 0, 8),
          // margin: EdgeInsets.fromLTRB(0, 0, , 0),
          child: _solutionItem(buildContext, s, width),
        );
      }).toList();
      return Column(
        children: widges,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
      );
    }
    return null;
  }

  _titleWithDesc(BuildContext context, String title, String desc) {
    return Container(
        margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _title(context, title),
            _desc(desc),
          ],
        ));
  }

  _title(BuildContext context, String title) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 4),
      child: Text(
        '$title: ',
        maxLines: 4,
        style: TextStyle(
          color: Theme.of(context).disabledColor,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }

  _desc(String desc) {
    if (desc == null || desc.trim().isEmpty) {
      desc = '暂无数据';
    }
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 4),
      child: Text(
        desc ?? '暂无数据',
        maxLines: 4,
        style: TextStyle(color: Colors.black87),
      ),
    );
  }

  _buildDangerWidgets(String danger) {
    List<Widget> dangers =
        danger.split('\r\n').map<Widget>((d) => _desc(d)).toList();
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: dangers,
    );
  }


  Widget _backgroundTitle(String title) {
      return Padding(
      child: Text(
        title,
        style: TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
      padding: EdgeInsets.fromLTRB(8, 8, 16, 8));
  }
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      
      children: <Widget>[
        _backgroundTitle('风险点描述'),
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(8, 8,0, 8),
          color: Colors.white,
          child: Text('暂无风险点描述'),
        ),
        _backgroundTitle('风险因素'),
        Container(
          width: MediaQuery.of(context).size.width,
          // color: Colors.white,
          child: _solutionWidget(context),
        ),
        // Container(
        //     width: MediaQuery.of(context).size.width,
        //     color: Colors.white,
        //     child: _solutionWidget(context)),
        // Container(
        //   color: Colors.white,
        //   width: MediaQuery.of(context).size.width,
        //   padding: EdgeInsets.fromLTRB(8, 16, 8, 8),
        //   child: _remarkArea(),
        // ),
        // Container(
        //   height: 48,
        //   child: _responseArea(),
        //   width: MediaQuery.of(context).size.width,
        //   color: Colors.blue,
        // )
      ],
    ));
  }
}
