import 'package:flutter/material.dart';
import 'package:safety_platform/models/hidden_danger.dart';
import 'package:safety_platform/utils.dart';

typedef OnHiddenDangerTap  =  void Function(HiddenDanger node);

class HiddenDangerItemWidget extends StatelessWidget {
  final HiddenDanger _hiddenDanger;

  final OnHiddenDangerTap _onTap;
  
  HiddenDangerItemWidget(this._hiddenDanger,this._onTap);


  Widget columnText(String text) {
    return Padding(
      padding: EdgeInsets.fromLTRB(16, 2, 8, 2),
      child: Text(text,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(color: Colors.white)),
    );
  }

  Widget build(BuildContext context) {
    HiddenDanger hd = this._hiddenDanger;
    return InkWell(
      onTap: () {
        // Navigator.of(context).push(MaterialPageRoute(builder: (_) => HiddenDangerDetailPage(hd)));
      },
      child: Card(
        elevation: 0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
        margin: EdgeInsets.fromLTRB(16, 0, 16, 8),
        child: Container(
          // padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
          ),
          child: Column(
            
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0, 8, 16, 8),
                  child: Text('隐患编号：${hd.id}'),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(2),
                  bottomRight: Radius.circular(2)),
                                  color: Theme.of(context).primaryColor,

                ),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                        child: Container(
                      // color: Colors.red,
                      margin: EdgeInsets.symmetric(vertical: 8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          columnText('更改人员'),
                          columnText('暂无数据'),
                          columnText('更新时间'),
                          columnText(
                              formatDate(hd.created,format:'yyyy-MM-dd')),
                        ],
                      ),
                      decoration: BoxDecoration(
                          border: Border(
                              right: BorderSide(
                        width: 0.5,
                        color: Colors.white,
                      ))),
                    )),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.symmetric(vertical: 8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            columnText('检查部位'),
                            columnText('${hd.risk.name}'),
                            columnText('隐患级别'),
                            columnText(hiddenDangerGradeCodeToText(hd.grade)),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

_buildRow(String title, String value) {
    if (value == null || value == 'null') {
      value = '';
    }
    return Container(
      padding: EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          bottom: BorderSide(
            width: 0.5,
            color: Color.fromARGB(255, 241, 241, 241),
          ),
        ),
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Text(title),
          ),
          Expanded(
            flex: 2,
            child: Text(
              value,
              textAlign: TextAlign.right,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(color: Colors.grey),
            ),
          )
        ],
      ),
    );
  }
class HiddenDangerLiteWidget extends StatelessWidget {
  HiddenDanger hiddenDanger;
  HiddenDangerLiteWidget(this.hiddenDanger);

  @override
  Widget build(BuildContext context) {
    return Container(
        // margin: EdgeInsets.only(bottom: 8.0),
        child: Column(
          children: <Widget>[
            _buildRow('隐患部位', hiddenDanger.risk.name),
            _buildRow('隐患类型', hiddenDanger.classification.name),
            _buildRow('隐患级别',hiddenDangerGradeCodeToText(hiddenDanger.grade)),
            /* Row(children: <Widget>[
            Expanded(
              child: Container(
                padding: EdgeInsets.all(16.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(
                    bottom: BorderSide(
                      width: 0.5,
                      color: Color.fromARGB(255, 241, 241, 241),
                    ),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Text('隐患描述'),
                    Padding(
                      child: Text(
                        '${hiddenDanger.memo}',
                        textAlign: TextAlign.left,
                        // overflow: TextOverflow.ellipsis,
                        style: TextStyle(color: Colors.grey),
                      ),
                      padding: EdgeInsets.only(top: 4),
                    ),
                  ],
                ),
              ),
            ),
          ]) */
          ],
        ));
  }
}