import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Button extends StatelessWidget {
  final Color buttonColor;

  final Color disableColor;

  final Color highlightColor;

  final Color textColor;

  final Widget loadingWidget;

  final bool loading;

  final Color loadingColor;

  final VoidCallback onPressed;

  final String text;

  final double textSize;

  const Button({
    this.buttonColor,
    this.disableColor,
    this.highlightColor,
    this.textColor,
    this.loadingWidget,
    this.loading = false,
    this.loadingColor,
    this.textSize,
    @required this.text,
    @required this.onPressed,
  });

  Widget build(BuildContext context) {
    Widget widget;

    if (loading) {
      if (loadingWidget != null) {
        widget = loadingWidget;
      } else {
        widget = SpinKitWave(
          color: loadingColor?? Theme.of(context).primaryColor,
          size: 18,
        );
      }
    } else {
      widget = Text(
        text,
        style: TextStyle(
          color: textColor ?? Theme.of(context).primaryColor,
          fontSize: textSize?? 14,
        ),
      );
    }
    return RaisedButton(
      color: buttonColor,
      disabledColor: disableColor,
      onPressed: () => onPressed(),
      child: widget,
    );
  }
}
