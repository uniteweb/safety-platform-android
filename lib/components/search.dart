import 'package:flutter/material.dart';


class Search2 extends StatelessWidget {

  @override
    Widget build(BuildContext context) {
      // TODO: implement build
      return null;
    }

}
class Search extends StatefulWidget {
  final ValueChanged<String> onSubmitted ;

  Search(this.onSubmitted);
  @override
  State<StatefulWidget> createState() => _SearchState();
}

class _SearchState extends State<Search> {
  TextEditingController _inputController;
  String text;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _inputController = TextEditingController();
    _inputController.addListener(() {
      String text = _inputController.text;
      print('text => $text, ');
      if (text.length <= 0) {
        text = null;
      }
      setState(() {
        this.text = text;
      });
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _inputController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      color: Color.fromARGB(255, 241, 241, 241),
      height: 56,
      padding: EdgeInsets.all(8),
      width: MediaQuery.of(context).size.width,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(6.0),
        ),
        child: TextField(
          autofocus: false,
          onSubmitted: (str)=> widget.onSubmitted(str),
          controller: _inputController,
          textInputAction: TextInputAction.search,
          // focusNode: FocusNode,
          decoration: InputDecoration(
            // hintText: 's',
            contentPadding: EdgeInsets.all(-20),
            fillColor: Colors.red,
            suffixIcon: this.text == null
                ? null
                : GestureDetector(
                    onTap: () => _inputController.clear(),
                    child: Icon(Icons.close,
                        // size: 30,
                        color: Colors.grey),
                  ),
            // suffixIcon: this.text != null
            // ?   Icon(
            //       Icons.close,
            //       size: 16,
            //     )

            // : null,
            prefixIcon: Icon(Icons.search),
            border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: const BorderRadius.all(Radius.circular(4.0))),
          ),
        ),
      ),
    );
  }
}
