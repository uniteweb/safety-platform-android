import 'package:flutter/material.dart';
import 'iconfont.dart';
import 'package:intl/intl.dart';

String riskLevelTransform(String level) {
  if (level == null) return level;

  switch (level) {
    case 'large':
      return '重大风险';
    case 'high':
      return '较大风险';
    case 'normal':
      return '一般风险';
    case 'low':
      return '较低风险';
  }
  return level;
}

String taskStatusTransform(String status) {
  switch (status) {
    case 'pending':
      return '未检查';
    case 'done':
      return '已完成';
    case 'risky':
      return '有风险';
  }
  return status;
}

Widget appLeading(BuildContext context) {
  return GestureDetector(
      onTap: () => Navigator.pop(context),
      child: Align(
          alignment: Alignment.centerLeft,
          child: Icon(
            MyIconFont.jiantou_zuo,
            size: 32,
          )));
}

String formatDate(DateTime date, {String format: 'yyyy-MM-dd HH:mm:ss'}) {
  print(
      '--${date.isUtc} -- ${date.timeZoneName} -- ${date.timeZoneOffset} -- ${date.toLocal()}');
  var formatter = DateFormat(format);
  if (date.isUtc) {
    return formatter.format(date.toLocal());
  }
  return formatter.format(date);
}

class SelectionNode {
  String code;
  String name;

  SelectionNode(this.code, this.name);
}

typedef OnSelectionCallback = void Function(SelectionNode node);
void showEditPopDialog(BuildContext context, TextEditingController controller,String title,
    String hitText, VoidCallback onPress) {
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          actions: <Widget>[
            // RaisedButton(child: Text('取消'),),
            FlatButton(
              onPressed: () {
                onPress();
                Navigator.of(context).pop();
              },
              // color: Theme.of(context).primaryColor,
              child: Text(
                '确认',
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                ),
              ),
            )
          ],
          title: Center(child: Text(title)),
          contentPadding: EdgeInsets.all(16),
          content: SingleChildScrollView(
            child: Container(
              color: Colors.white,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      // Expanded(flex: 1, child: Text('整改描述')),
                      Expanded(
                        flex: 2,
                        child: TextField(
                          controller: controller,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(8),
                            hintText: hitText,
                            hintStyle: TextStyle(
                              fontSize: 14,
                            ),
                            border: InputBorder.none,
                          ),
                          maxLength: 150,
                          maxLines: 5,
                          textInputAction: TextInputAction.done,
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      });
}

void showErrorDialog(
    BuildContext context, String title, String content, VoidCallback onPress) {
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: <Widget>[
            // FlatButton(
            //   onPressed: () => Navigator.of(context).pop(),
            //   child: Text('取消'),
            // ),
            FlatButton(
              onPressed: () {
                Navigator.of(context).pop();
                onPress();
              },
              // textColor: Theme.of(context).primaryColor,
              child: Text(
                '确定',
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                ),
              ),
            )
          ],
        );
      });
}

void openSingleSelectionDialog(
  BuildContext context,
  List<SelectionNode> children,
  OnSelectionCallback callback, {
  String title,
}) {
  showDialog(
      context: context,
      builder: (context) {
        return _singleListDialog(context, children, callback);
      });
}

Widget _singleListDialog(
  BuildContext context,
  List<SelectionNode> children,
  OnSelectionCallback callback, {
  String title,
}) {
  Widget titleWidget;
  if (title != null) {
    titleWidget = Container(
      width: MediaQuery.of(context).size.width,
      child: Center(child: Text('隐��级别')),
    );
  }
  return SimpleDialog(
      title: titleWidget,
      contentPadding: EdgeInsets.all(0),
      children: <Widget>[
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: children.map((f) {
            return GestureDetector(
              onTap: () {
                Navigator.pop(context);
                if (callback != null) {
                  callback(f);
                }
              },
              child: Container(
                // height: 50,
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      width: 0.5,
                      color: Color.fromARGB(255, 241, 241, 241),
                    ),
                  ),
                ),
                child: Text(f.name),
              ),
            );
          }).toList(),
        ),
      ]);
}
