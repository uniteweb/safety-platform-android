import 'package:flutter/material.dart';
import 'package:qrcode_reader/qrcode_reader.dart';

import 'package:barcode_scan/barcode_scan.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:safety_platform/components/search.dart';
import 'package:safety_platform/components/tree_selection.dart';

class Playground extends StatefulWidget {
  @override
  PlaygroundState createState() => new PlaygroundState();

  BuildContext parentContext;
  Playground({this.parentContext});
}

class PlaygroundState extends State<Playground> {
  @override
  Widget build(BuildContext context) {
    return 
        
       Scaffold(
         body:WillPopScope(
        child: Container(
          child: RaisedButton(
            child: Text('click me'),
            onPressed: () {
              showDialog(
                context: context,
                builder: (_) {
                 return SimpleDialog(
                   contentPadding: EdgeInsets.all(0),
                   children: [ Container()]);
                }
              );
            },
          )

          //       scan();
          // QRCodeReader()
          //     .setAutoFocusIntervalInMs(200) // default 5000
          //     .setForceAutoFocus(true) // default false
          //     .setTorchEnabled(true) // default false
          //     .setHandlePermissions(true) // default true
          //     .setExecuteAfterPermissionGranted(true) // default true
          //     .scan()
          //     .then((qrcode) {
          //   print(qrcode);
          // });
        ),
        // onWillPop: ()=> Future<bool>.value(false),
         ),
    );
  }

  Future scan() async {
    try {
      String barcode = await BarcodeScanner.scan();
      print(barcode);
      // setState(() {
      //   return this.barcode = barcode;
      // });
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        print('The user did not grant the camera permission!');
      } else {
        print('The user did not grant the camera !');
      }
    } on FormatException {
      print(
          'null (User returned using the "back"-button before scanning anything. Result)');
    } catch (e) {
      print('Unknown error $e');
    }
  }
}
