import 'package:flutter/material.dart';
import 'iconfont.dart';
import 'package:flutter/rendering.dart' show debugPaintSizeEnabled;
import 'app.dart';
import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:safety_platform/pages/login/index.dart';
import 'package:safety_platform/authentication.dart';
import 'package:bloc/bloc.dart' show BlocSupervisor,BlocDelegate;

class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onTransition(trans) {
    print(trans);
  }
}
void main() {
  // debugPaintSizeEnabled = true;
ErrorWidget.builder = (FlutterErrorDetails flutterErrorDetails){
        print(flutterErrorDetails.toString());
        return Center(
          child: Text("Flutter 走神了"),
          );
    };
    BlocSupervisor().delegate = SimpleBlocDelegate();

  runZoned(
      () => runApp(
            MyApp(),
          ), onError: (Object obj, StackTrace stackobj) {
    print('捕获到崩溃!!\r\n 来源: [$obj] \r\n stack: $stackobj');
  });
}


class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  Future<bool> isNeedLogin() async {
    bool user = await getUser();
    print(" ==> $user");
    return user;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      
      routes: {
        'main': (_) => App(),
      },
      home: Scaffold(
        resizeToAvoidBottomPadding: false,
        body: FutureBuilder(
            future: isNeedLogin(),
            builder: (BuildContext context, AsyncSnapshot<bool> sanpshop) {
              switch (sanpshop.connectionState) {
                case ConnectionState.active:
                case ConnectionState.none:
                case ConnectionState.waiting:
                  {
                    print("ghd d  ${sanpshop.connectionState}");
                    return Center(child: Text("正在加载"));
                  }
                case ConnectionState.done:
                  {
                    bool isNeedLogin = sanpshop.data;
                    if (isNeedLogin) {
                      return LoginPage();
                    } else {
                      return App();
                    }
                  }
              }
            }),
      ),
    );
//        home: Scaffold(backgroundColor: Colors.black, body: LoginPage()));
  }
}

class LoginCard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginCardState();
  }
}

class _LoginCardState extends State<LoginCard> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height,
      child: Padding(
        padding: EdgeInsets.all(24.0),
        child: buildCard(),
      ),
    );
  }

  Widget buildCard() {
    return Card(
      color: Colors.yellow[500],
      child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Center(
              child: Column(
            // Column is also layout widget. It takes a list of children and
            // arranges them vertically. By default, it sizes itself to fit its
            // children horizontally, and tries to be as tall as its parent.
            //
            // Invoke "debug painting" (press "p" in the console, choose the
            // "Toggle Debug Paint" action from the Flutter Inspector in Android
            // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
            // to see the wireframe for each widget.
            //
            // Column has various properties to control how it sizes itself and
            // how it positions its children. Here we use mainAxisAlignment to
            // center the children vertically; the main axis here is the vertical
            // axis because Columns are vertical (the cross axis would be
            // horizontal).
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
//                Icon(MyIconFont.phone),

              TextField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10.0),
                  icon: Icon(
                    MyIconFont.phone,
                    size: 24,
                  ),
//                    labelText: '请输入你的用户名',
//                    helperText: '请输入注册的手机号',
                  hintText: '请你输入手机号',
                ),
                autofocus: false,
              ),
              TextField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10.0),
                  icon: Icon(
                    MyIconFont.password,
                    size: 24,
                  ),
//                    labelText: '请输入密码',
                  hintText: '请您输入密码',
                ),
                autofocus: false,
              ),
              Expanded(
                child: RaisedButton(
                  onPressed: () {
                    print(
                        '点击3333333333333333333333333333333333333333333333333333333333333333333333333333333');
                  },

                  color: Colors.blueAccent,
                  //按钮的背景颜色
                  // padding: EdgeInsets.all(50.0),//按钮距离里面内容的内边距
                  child: new Text('登录'),
                  textColor: Colors.white,
                  //文字的颜色
                  textTheme: ButtonTextTheme.normal,
                  //按钮的主题
                  onHighlightChanged: (bool b) {
                    //水波纹高亮变化回调
                  },
                  //        disabledTextColor: Colors.black54,//按钮禁用时候文字的颜色
                  //       disabledColor: Colors.black54,//按钮被禁用的时候显示的颜色
                  //       highlightColor: Colors.yellowAccent,//点击或者toch控件高亮的时候显示在控件上面，水波纹下面的颜色
                  //        splashColor: Colors.white,//水波纹的颜色
                  //        colorBrightness: Brightness.light,//按钮主题高亮
                  elevation: 10.0,
                  //按钮下面的阴影
                  highlightElevation: 10.0,
                  //高亮时候的阴影
                  disabledElevation: 10.0, //按下的时候的阴影
//              shape:,//设置形状  LearnMaterial中有详解
                ),
              ),
            ],
          ))),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter += 4;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
//      appBar: AppBar(
//        // Here we take the value from the MyHomePage object that was created by
//        // the App.build method, and use it to set our appbar title.
//        title: Text(widget.title),
//      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
