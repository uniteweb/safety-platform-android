// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'advertisement.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Advertisement _$AdvertisementFromJson(Map<String, dynamic> json) {
  return Advertisement()
    ..id = json['id'] as String
    ..content = json['content'] == null
        ? null
        : AdvertisementContent.fromJson(json['content'] as Map<String, dynamic>)
    ..localPath = json['localPath'] as String;
}

Map<String, dynamic> _$AdvertisementToJson(Advertisement instance) =>
    <String, dynamic>{
      'id': instance.id,
      'content': instance.content,
      'localPath': instance.localPath
    };

AdvertisementContent _$AdvertisementContentFromJson(Map<String, dynamic> json) {
  return AdvertisementContent()
    ..imageUrl = json['image_url'] as String
    ..title = json['title'] as String
    ..action = json['action'] as String
    ..actionValue = json['action_value'] as String
    ..actionAttr = json['action_attr'] as String;
}

Map<String, dynamic> _$AdvertisementContentToJson(
        AdvertisementContent instance) =>
    <String, dynamic>{
      'image_url': instance.imageUrl,
      'title': instance.title,
      'action': instance.action,
      'action_value': instance.actionValue,
      'action_attr': instance.actionAttr
    };

AdvertisementPage _$AdvertisementPageFromJson(Map<String, dynamic> json) {
  return AdvertisementPage(
      json['count'] as int,
      json['next'] as int,
      json['previous'] as int,
      (json['results'] as List)
          ?.map((e) => e == null
              ? null
              : Advertisement.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$AdvertisementPageToJson(AdvertisementPage instance) =>
    <String, dynamic>{
      'count': instance.count,
      'next': instance.next,
      'previous': instance.previous,
      'results': instance.results
    };
