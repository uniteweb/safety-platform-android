// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'received_notification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReceivedNotification _$ReceivedNotificationFromJson(Map<String, dynamic> json) {
  return ReceivedNotification(
      json['id'] as int,
      json['signature_time'] == null
          ? null
          : DateTime.parse(json['signature_time'] as String),
      json['read_time'] == null
          ? null
          : DateTime.parse(json['read_time'] as String),
      json['updated'] == null
          ? null
          : DateTime.parse(json['updated'] as String),
      json['status'] as String,
      json['read'] as bool,
      json['signed'] as bool,
      json['created'] == null
          ? null
          : DateTime.parse(json['created'] as String),
      json['notification'] == null
          ? null
          : Notification.fromJson(
              json['notification'] as Map<String, dynamic>));
}

Map<String, dynamic> _$ReceivedNotificationToJson(
        ReceivedNotification instance) =>
    <String, dynamic>{
      'id': instance.id,
      'signature_time': instance.signatureTime?.toIso8601String(),
      'read_time': instance.readTime?.toIso8601String(),
      'updated': instance.updated?.toIso8601String(),
      'created': instance.created?.toIso8601String(),
      'notification': instance.notification,
      'status': instance.status,
      'read': instance.read,
      'signed': instance.signed
    };
