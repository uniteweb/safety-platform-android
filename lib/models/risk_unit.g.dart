// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'risk_unit.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RiskUnitPage _$RiskUnitPageFromJson(Map<String, dynamic> json) {
  return RiskUnitPage()
    ..count = json['count'] as int
    ..next = json['next'] as int
    ..previous = json['previous'] as int
    ..results = (json['results'] as List)
        ?.map((e) =>
            e == null ? null : RiskUnit.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$RiskUnitPageToJson(RiskUnitPage instance) =>
    <String, dynamic>{
      'count': instance.count,
      'next': instance.next,
      'previous': instance.previous,
      'results': instance.results
    };

RiskUnit _$RiskUnitFromJson(Map<String, dynamic> json) {
  return RiskUnit(
      json['id'] as int,
      json['unit_code'] as String,
      json['unit_name'] as String,
      json['category'] == null
          ? null
          : UnitCategory.fromJson(json['category'] as Map<String, dynamic>),
      json['cycle_type'] as String,
      json['cycle'] as int,
      json['status'] as String,
      (json['risks'] as List)
          ?.map((e) =>
              e == null ? null : Risk.fromJson(e as Map<String, dynamic>))
          ?.toList())
    ..riskCount = json['risk_count'] as int
    ..created = json['created'] == null
        ? null
        : DateTime.parse(json['created'] as String);
}

Map<String, dynamic> _$RiskUnitToJson(RiskUnit instance) => <String, dynamic>{
      'id': instance.id,
      'unit_code': instance.unitCode,
      'unit_name': instance.unitName,
      'cycle_type': instance.cycleType,
      'category': instance.category,
      'risks': instance.risks,
      'risk_count': instance.riskCount,
      'cycle': instance.cycle,
      'status': instance.status,
      'created': instance.created?.toIso8601String()
    };
