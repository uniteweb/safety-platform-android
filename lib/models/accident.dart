import 'package:json_annotation/json_annotation.dart';
import 'package:safety_platform/models/unit_category.dart';

part 'accident.g.dart';

///这个标注是告诉生成器，这个类是需要生成Model类的
@JsonSerializable()
class Accident{
  int id;
  String code;
  String name;
  String description;


  Accident(this.id,this.code,this.name,this.description);


   //不同的类使用不同的mixin即可
  factory Accident.fromJson(Map<String, dynamic> json) => _$AccidentFromJson(json);
  Map<String, dynamic> toJson() => _$AccidentToJson(this);

  @override
    String toString() {
      return this.name;
    }
}