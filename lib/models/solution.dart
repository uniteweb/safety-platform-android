import 'package:json_annotation/json_annotation.dart';
part 'solution.g.dart';

///这个标注是告诉生成器，这个类是需要生成Model类的
@JsonSerializable()
class Solution {
  int id;
  String title;
  int order;

  Solution(this.id,this.title,this.order);
  //不同的类使用不同的mixin即可
  factory Solution.fromJson(Map<String, dynamic> json) => _$SolutionFromJson(json);
  Map<String, dynamic> toJson() => _$SolutionToJson(this);

  @override
  String toString() => this.title;
}