import 'package:json_annotation/json_annotation.dart';
import 'package:safety_platform/models/user.dart';
import 'package:safety_platform/models/attachment.dart';
import 'package:safety_platform/models/risk.dart';
import 'dart:convert';

part 'task.g.dart';

///这个标注是告诉生成器，这个类是需要生成Model类的
@JsonSerializable()
class TaskPage extends Object {
  int count;
  String next;
  String previous;

  List<Task> results;

  TaskPage(this.count, this.next, this.previous);



  //不同的类使用不同的mixin即可
  factory TaskPage.fromJson(Map<String, dynamic> json) =>
      _$TaskPageFromJson(json);
  Map<String, dynamic> toJson() => _$TaskPageToJson(this);

  @override
  String toString() {
    return 'count:${this.count}, previous:${this.previous},next:${this.next},results:${this.results}';
  }
}

@JsonSerializable()
class TaskItemGroup {
  int id;

  String title;

  int parent;

  TaskItemGroup();

  factory TaskItemGroup.fromJson(dynamic json) => _$TaskItemGroupFromJson(json);

  Map<String,dynamic> toJson() => _$TaskItemGroupToJson(this);
}


///这个标注是告诉生成器，这个类是需要生成Model类的

@JsonSerializable()
class TaskItem {
  TaskItem();
  @JsonKey(name: 'id')
  String id;

  @JsonKey(name: 'result')
  String result;

  @JsonKey(name: 'remark')
  String remark;

  @JsonKey(name: 'order')
  int order;

  @JsonKey(name: 'content')
  TaskItemContent content;

  List<Attachment> attachments;

  @override
  String toString() {
    return json.encode({
      'id': id,
      'result':result,
      'remark':remark,
      'attachments':attachments
    });
  }

  factory TaskItem.fromJson(json) => _$TaskItemFromJson(json);

  Map<String,dynamic> toJson() =>  _$TaskItemToJson(this);
}
@JsonSerializable()
class TaskItemContent{

  TaskItemContent();
  @JsonKey(name:'id')
  int id;

  @JsonKey(name: 'content')
  String content;

  @JsonKey(name: 'check_way')
  String checkWay;

  @JsonKey(name: 'risk_level')
  String riskkLevel;

  @JsonKey(name:'negative_detail')
  String negativeDetail;

  @JsonKey(name: 'law')
  String law;

  @JsonKey(name: 'comment')
  String comment;

  @JsonKey(name: 'answers')
  String answers;

  @JsonKey(name: 'right_answer')
  String rightAnswer;

  @JsonKey(name: 'can_ignore')
  bool canIgnore;

  @JsonKey(name: 'standard')
  String  standard;

  @JsonKey(name: 'category')
  TaskItemGroup category;


  factory TaskItemContent.fromJson(Map<String,dynamic> json) => _$TaskItemContentFromJson(json);

  Map<String,dynamic> toJson() => _$TaskItemContentToJson(this);
}
@JsonSerializable()
class TaskExtension{

  TaskExtension();
  @JsonKey(name: 'risk_id')
  int riskId;

  @JsonKey(name: 'risk_name')
  String riskName;

  factory TaskExtension.fromJson(Map<String,dynamic> json) => _$TaskExtensionFromJson(json);

  Map<String,dynamic> toJson() => _$TaskExtensionToJson(this);
}
@JsonSerializable()
class Task {

    static String transStatus(String status) {
    switch(status) {
      case 'pending': return '未检查';
      case 'done': return '已完成';
      case 'risk': return '有风险';
    }
    return status;

  }
  Task();
  String id;

  // Risk risk;
  String status;

  @JsonKey(name: 'type', )
  String type;


  @JsonKey(name: 'name')
  String name;

  @JsonKey(name: 'extension')
  TaskExtension extension;

  @JsonKey(name: 'created')
  DateTime created;

  @JsonKey(name: 'updated')
  DateTime updated;

  @JsonKey(name: 'result')
  String result;

  @JsonKey(name: 'owner')
  User owner;
  
  @JsonKey(name: 'description')
  String description;

  @JsonKey(name: 'time_published')
  DateTime timePublished;

  @JsonKey(name: 'items')
  List<TaskItem> items;
  // @JsonKey(name: 'time_of_expiration')
  // String timeOfExpiration;

  // @JsonKey(name: 'time_of_start')
  // String timeOfStart;

  // @JsonKey(name: 'time_of_finish')
  // String timeOfFinish;

  // DateTime created;

  // DateTime updated;

  // List<TaskItem> items;

  // @JsonKey(name: 'user_of_execution')
  // User userOfExecution;

  // Task(this.id, this.risk, this.status, this.timeOfExpiration, this.items,
  //     this.timeOfFinish, this.timeOfStart, this.created, this.updated);

  //不同的类使用不同的mixin即可
  factory Task.fromJson(Map<String, dynamic> json) => _$TaskFromJson(json);
  Map<String, dynamic> toJson() => _$TaskToJson(this);

  
  @override
  String toString() {
    return this.id;
  }
}
