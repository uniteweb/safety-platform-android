
import 'package:json_annotation/json_annotation.dart';



part 'login_user.g.dart';

@JsonSerializable()
class LoginUser{

  LoginUser();

  
  String profile;
  String username;
  String phone;
  int id ;
  String role;
  @JsonKey(name:'screen_name')
  String screenName;
  List<String> permissions;

  bool hasPerm(String permission) {
    return permissions.any((perm) =>perm == permission);
  }
  factory LoginUser.fromJson(Map<String,dynamic> json) => _$LoginUserFromJson(json);
}
