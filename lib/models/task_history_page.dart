import 'package:json_annotation/json_annotation.dart';
import 'package:safety_platform/models/task_history.dart';
import 'dart:convert';
part 'task_history_page.g.dart';

@JsonSerializable()
class TaskHistoryPage {
  int count;

  List<TaskHistory> results;

  @override
  String toString() {
    return json.encode(this.toJson());
  }

  TaskHistoryPage(this.count, this.results);

  //不同的类使用不同的mixin即可
  factory TaskHistoryPage.fromJson(Map<String, dynamic> json) =>
      _$TaskHistoryPageFromJson(json);
  Map<String, dynamic> toJson() => _$TaskHistoryPageToJson(this);
}
