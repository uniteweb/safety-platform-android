// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'accident.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Accident _$AccidentFromJson(Map<String, dynamic> json) {
  return Accident(json['id'] as int, json['code'] as String,
      json['name'] as String, json['description'] as String);
}

Map<String, dynamic> _$AccidentToJson(Accident instance) => <String, dynamic>{
      'id': instance.id,
      'code': instance.code,
      'name': instance.name,
      'description': instance.description
    };
