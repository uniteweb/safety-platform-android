// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'received_notification_page.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReceivedNotificationPage _$ReceivedNotificationPageFromJson(
    Map<String, dynamic> json) {
  return ReceivedNotificationPage(
      json['count'] as int,
      json['next'] as int,
      json['previous'] as int,
      (json['results'] as List)
          ?.map((e) => e == null
              ? null
              : ReceivedNotification.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$ReceivedNotificationPageToJson(
        ReceivedNotificationPage instance) =>
    <String, dynamic>{
      'count': instance.count,
      'next': instance.next,
      'previous': instance.previous,
      'results': instance.results
    };
