import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
import 'package:safety_platform/models/attachment.dart';

part 'task_item.g.dart';

@JsonSerializable()
class TaskItem {
  String id;

  String content;

  @JsonKey(name: 'right_answer')
  String rightAnswer;

  String result;

  String remark;

  String answers;
  @JsonKey(name: 'time_committed')
  DateTime timeCommitted;

  bool showRemark = false;

  List<Attachment> attachments;

  String hiddenDangerId ;

  @override
  String toString() {
    return json.encode(this.toJson());
  }

  TaskItem(this.id, this.showRemark, this.content, this.rightAnswer,
      this.remark, this.answers, this.timeCommitted, this.attachments);

  //不同的类使用不同的mixin即可
  factory TaskItem.fromJson(Map<String, dynamic> json) =>
      _$TaskItemFromJson(json);
  Map<String, dynamic> toJson() => _$TaskItemToJson(this);
}
