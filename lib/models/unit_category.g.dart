// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'unit_category.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UnitCategory _$UnitCategoryFromJson(Map<String, dynamic> json) {
  return UnitCategory(json['id'] as int, json['title'] as String,
      json['prefix'] as String, json['description'] as String);
}

Map<String, dynamic> _$UnitCategoryToJson(UnitCategory instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'prefix': instance.prefix
    };
