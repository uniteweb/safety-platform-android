import 'package:json_annotation/json_annotation.dart';
import 'package:safety_platform/models/task.dart';
import 'package:safety_platform/models/unit_category.dart';
// user.g.dart 将在我们运行生成命令后自动生成
part 'task_risk_unit.g.dart';

///这个标注是告诉生成器，这个类是需要生成Model类的
@JsonSerializable()
class TaskRiskUnit {

  TaskRiskUnit(this.id,this.unitCode, this.category, this.unitName,this.tasks,this.cycleType,this.cycle,this.status,);
  int id;
  @JsonKey(name:'unit_code')
  String unitCode;

  @JsonKey(name:'unit_name')
  String unitName;

  @JsonKey(name:'cycle_type')
  String cycleType;

 UnitCategory category;

  List<Task> tasks;

  int cycle;

  String status;

  DateTime created;

  //不同的类使用不同的mixin即可
  factory TaskRiskUnit.fromJson(Map<String, dynamic> json) => _$TaskRiskUnitFromJson(json);
  Map<String, dynamic> toJson() => _$TaskRiskUnitToJson(this);

  @override
    String toString() {
      // TODO: implement toString
      return 'id:${this.id}, unitCode:${this.unitCode}, unitName:${this.unitName}';
    }
}