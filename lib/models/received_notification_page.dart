import 'package:json_annotation/json_annotation.dart';
import 'package:safety_platform/models/received_notification.dart';

part 'received_notification_page.g.dart';

@JsonSerializable()
class ReceivedNotificationPage {
  int count;
  int next;
  int previous;

  List<ReceivedNotification> results;

  ReceivedNotificationPage(this.count, this.next, this.previous, this.results);

  // @JsonKey(name: 'signature_user')

  // @JsonKey(name: 'signature_user')
  //不同的类使用不同的mixin即可
  factory ReceivedNotificationPage.fromJson(Map<String, dynamic> json) =>
      _$ReceivedNotificationPageFromJson(json);
  Map<String, dynamic> toJson() => _$ReceivedNotificationPageToJson(this);
}
