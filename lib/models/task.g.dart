// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskPage _$TaskPageFromJson(Map<String, dynamic> json) {
  return TaskPage(
      json['count'] as int, json['next'] as String, json['previous'] as String)
    ..results = (json['results'] as List)
        ?.map(
            (e) => e == null ? null : Task.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$TaskPageToJson(TaskPage instance) => <String, dynamic>{
      'count': instance.count,
      'next': instance.next,
      'previous': instance.previous,
      'results': instance.results
    };

TaskItemGroup _$TaskItemGroupFromJson(Map<String, dynamic> json) {
  return TaskItemGroup()
    ..id = json['id'] as int
    ..title = json['title'] as String
    ..parent = json['parent'] as int;
}

Map<String, dynamic> _$TaskItemGroupToJson(TaskItemGroup instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'parent': instance.parent
    };

TaskItem _$TaskItemFromJson(Map<String, dynamic> json) {
  return TaskItem()
    ..id = json['id'] as String
    ..result = json['result'] as String
    ..remark = json['remark'] as String
    ..order = json['order'] as int
    ..content = json['content'] == null
        ? null
        : TaskItemContent.fromJson(json['content'] as Map<String, dynamic>)
    ..attachments = (json['attachments'] as List)
        ?.map((e) =>
            e == null ? null : Attachment.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$TaskItemToJson(TaskItem instance) => <String, dynamic>{
      'id': instance.id,
      'result': instance.result,
      'remark': instance.remark,
      'order': instance.order,
      'content': instance.content,
      'attachments': instance.attachments
    };

TaskItemContent _$TaskItemContentFromJson(Map<String, dynamic> json) {
  return TaskItemContent()
    ..id = json['id'] as int
    ..content = json['content'] as String
    ..checkWay = json['check_way'] as String
    ..riskkLevel = json['risk_level'] as String
    ..negativeDetail = json['negative_detail'] as String
    ..law = json['law'] as String
    ..comment = json['comment'] as String
    ..answers = json['answers'] as String
    ..rightAnswer = json['right_answer'] as String
    ..canIgnore = json['can_ignore'] as bool
    ..standard = json['standard'] as String
    ..category = json['category'] == null
        ? null
        : TaskItemGroup.fromJson(json['category']);
}

Map<String, dynamic> _$TaskItemContentToJson(TaskItemContent instance) =>
    <String, dynamic>{
      'id': instance.id,
      'content': instance.content,
      'check_way': instance.checkWay,
      'risk_level': instance.riskkLevel,
      'negative_detail': instance.negativeDetail,
      'law': instance.law,
      'comment': instance.comment,
      'answers': instance.answers,
      'right_answer': instance.rightAnswer,
      'can_ignore': instance.canIgnore,
      'standard': instance.standard,
      'category': instance.category
    };

TaskExtension _$TaskExtensionFromJson(Map<String, dynamic> json) {
  return TaskExtension()
    ..riskId = json['risk_id'] as int
    ..riskName = json['risk_name'] as String;
}

Map<String, dynamic> _$TaskExtensionToJson(TaskExtension instance) =>
    <String, dynamic>{
      'risk_id': instance.riskId,
      'risk_name': instance.riskName
    };

Task _$TaskFromJson(Map<String, dynamic> json) {
  return Task()
    ..id = json['id'] as String
    ..status = json['status'] as String
    ..type = json['type'] as String
    ..name = json['name'] as String
    ..extension = json['extension'] == null
        ? null
        : TaskExtension.fromJson(json['extension'] as Map<String, dynamic>)
    ..created = json['created'] == null
        ? null
        : DateTime.parse(json['created'] as String)
    ..updated = json['updated'] == null
        ? null
        : DateTime.parse(json['updated'] as String)
    ..result = json['result'] as String
    ..owner = json['owner'] == null
        ? null
        : User.fromJson(json['owner'] as Map<String, dynamic>)
    ..description = json['description'] as String
    ..timePublished = json['time_published'] == null
        ? null
        : DateTime.parse(json['time_published'] as String)
    ..items = (json['items'] as List)
        ?.map((e) => e == null ? null : TaskItem.fromJson(e))
        ?.toList();
}

Map<String, dynamic> _$TaskToJson(Task instance) => <String, dynamic>{
      'id': instance.id,
      'status': instance.status,
      'type': instance.type,
      'name': instance.name,
      'extension': instance.extension,
      'created': instance.created?.toIso8601String(),
      'updated': instance.updated?.toIso8601String(),
      'result': instance.result,
      'owner': instance.owner,
      'description': instance.description,
      'time_published': instance.timePublished?.toIso8601String(),
      'items': instance.items
    };
