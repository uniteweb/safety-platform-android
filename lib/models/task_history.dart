import 'package:json_annotation/json_annotation.dart';
import 'package:safety_platform/models/task.dart';
import 'dart:convert';
part 'task_history.g.dart';
@JsonSerializable()
class TaskHistory{

  DateTime date;
  List<Task> tasks;


   @override
  String toString() {
    return json.encode(this.toJson());
  }


  TaskHistory(this.date, this.tasks);

  //不同的类使用不同的mixin即可
  factory TaskHistory.fromJson(Map<String, dynamic> json) =>
      _$TaskHistoryFromJson(json);
  Map<String, dynamic> toJson() => _$TaskHistoryToJson(this);
}