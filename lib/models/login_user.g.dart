// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginUser _$LoginUserFromJson(Map<String, dynamic> json) {
  return LoginUser()
    ..profile = json['profile'] as String
    ..username = json['username'] as String
    ..phone = json['phone'] as String
    ..id = json['id'] as int
    ..role = json['role'] as String
    ..screenName = json['screen_name'] as String
    ..permissions =
        (json['permissions'] as List)?.map((e) => e as String)?.toList();
}

Map<String, dynamic> _$LoginUserToJson(LoginUser instance) => <String, dynamic>{
      'profile': instance.profile,
      'username': instance.username,
      'phone': instance.phone,
      'id': instance.id,
      'role': instance.role,
      'screen_name': instance.screenName,
      'permissions': instance.permissions
    };
