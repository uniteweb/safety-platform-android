// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskItem _$TaskItemFromJson(Map<String, dynamic> json) {
  return TaskItem(
      json['id'] as String,
      json['showRemark'] as bool,
      json['content'] as String,
      json['right_answer'] as String,
      json['remark'] as String,
      json['answers'] as String,
      json['time_committed'] == null
          ? null
          : DateTime.parse(json['time_committed'] as String),
      (json['attachments'] as List)
          ?.map((e) =>
              e == null ? null : Attachment.fromJson(e as Map<String, dynamic>))
          ?.toList())
    ..result = json['result'] as String
    ..hiddenDangerId = json['hiddenDangerId'] as String;
}

Map<String, dynamic> _$TaskItemToJson(TaskItem instance) => <String, dynamic>{
      'id': instance.id,
      'content': instance.content,
      'right_answer': instance.rightAnswer,
      'result': instance.result,
      'remark': instance.remark,
      'answers': instance.answers,
      'time_committed': instance.timeCommitted?.toIso8601String(),
      'showRemark': instance.showRemark,
      'attachments': instance.attachments,
      'hiddenDangerId': instance.hiddenDangerId
    };
