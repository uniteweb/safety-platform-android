// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_list_page.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskListPage _$TaskListPageFromJson(Map<String, dynamic> json) {
  return TaskListPage(
      json['count'] as int,
      json['next'] as String,
      json['previous'] as String,
      (json['results'] as List)
          ?.map((e) =>
              e == null ? null : Task.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$TaskListPageToJson(TaskListPage instance) =>
    <String, dynamic>{
      'count': instance.count,
      'next': instance.next,
      'previous': instance.previous,
      'results': instance.results
    };
