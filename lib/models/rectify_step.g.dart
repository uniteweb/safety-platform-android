// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rectify_step.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RectifyStep _$RectifyStepFromJson(Map<String, dynamic> json) {
  return RectifyStep()
    ..id = json['id'] as int
    ..title = json['title'] as String
    ..type = json['type'] as String
    ..status = json['status'] as String;
}

Map<String, dynamic> _$RectifyStepToJson(RectifyStep instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'type': instance.type,
      'status': instance.status
    };
