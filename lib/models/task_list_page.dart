import 'package:json_annotation/json_annotation.dart';
import 'package:safety_platform/models/task.dart';
// user.g.dart 将在我们运行生成命令后自动生成
part 'task_list_page.g.dart';

///这个标注是告诉生成器，这个类是需要生成Model类的
@JsonSerializable()
class TaskListPage extends Object {
  int count;
  String next;
  String previous;

  List<Task> results;

  TaskListPage(this.count, this.next, this.previous,this.results);


  static String transStatus(String status) {
    switch(status) {
      case 'pending': return '未检查';
      case 'done': return '已完成';
      case 'risk': return '有风险';
    }
    return status;

  }
  //不同的类使用不同的mixin即可
  factory TaskListPage.fromJson(Map<String, dynamic> json) =>
      _$TaskListPageFromJson(json);
  Map<String, dynamic> toJson() => _$TaskListPageToJson(this);

  @override
  String toString() {
    return 'count:${this.count}, previous:${this.previous},next:${this.next},results:${this.results}';
  }
}
