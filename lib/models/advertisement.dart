import 'package:json_annotation/json_annotation.dart';

part 'advertisement.g.dart';

@JsonSerializable()
class Advertisement{
  Advertisement();
  String id ;
  
  AdvertisementContent content;
  String localPath;

  factory Advertisement.fromJson(Map<String,dynamic> json) => _$AdvertisementFromJson(json);

  Map<String,dynamic> toJson() => _$AdvertisementToJson(this);
}

@JsonSerializable()
class AdvertisementContent {
  AdvertisementContent();
  @JsonKey(name: 'image_url')
  String imageUrl;

  String title;

  String action;

  @JsonKey(name: 'action_value')
  String actionValue;

  @JsonKey(name: 'action_attr')
  String actionAttr;

  factory AdvertisementContent.fromJson(Map<String,dynamic> json) => _$AdvertisementContentFromJson(json);

  Map<String,dynamic> toJson() => _$AdvertisementContentToJson(this);
}
@JsonSerializable()
class AdvertisementPage {
  int count;
  int next;
  int previous;
  List<Advertisement> results;

  AdvertisementPage(this.count, this.next, this.previous, this.results);


     //不同的类使用不同的mixin即可
  factory AdvertisementPage.fromJson(Map<String, dynamic> json) => _$AdvertisementPageFromJson(json);
  Map<String, dynamic> toJson() => _$AdvertisementPageToJson(this);
}
