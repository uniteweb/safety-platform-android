import 'package:json_annotation/json_annotation.dart';

part 'message.g.dart';

@JsonSerializable()
class Message {
  int id;
  String content;
  String type;
  bool read;
  DateTime updated;
  DateTime created;

  Message();

  factory Message.fromJson(json) => _$MessageFromJson(json);

  Map<String, dynamic> toJson() => _$MessageToJson(this);
}

@JsonSerializable()
class MessagePage {
int count;
  int next;
  int previous;

  MessagePage() {}
  List<Message> results;
  factory MessagePage.fromJson(json) => _$MessagePageFromJson(json);

  Map<String, dynamic> toJson() => _$MessagePageToJson(this);
}
