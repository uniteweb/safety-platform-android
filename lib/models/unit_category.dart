import 'package:json_annotation/json_annotation.dart';

// user.g.dart 将在我们运行生成命令后自动生成
part 'unit_category.g.dart';

///这个标注是告诉生成器，这个类是需要生成Model类的
@JsonSerializable()
class UnitCategory {
  int  id;

  String title;

  String description;


  String prefix;
  UnitCategory(this.id,this.title, this.prefix, this.description);


   //不同的类使用不同的mixin即可
  factory UnitCategory.fromJson(Map<String, dynamic> json) => _$UnitCategoryFromJson(json);
  Map<String, dynamic> toJson() => _$UnitCategoryToJson(this);

}