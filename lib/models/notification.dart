import 'package:json_annotation/json_annotation.dart';
import 'package:safety_platform/models/user.dart';
part 'notification.g.dart';

@JsonSerializable()
class Notification {
  int id;

  String title;

  String content;

  String type;

  @JsonKey(name: 'need_signature')
  bool needSignature;

  @JsonKey(name: 'date_published')
  DateTime datePublished;

  @JsonKey(name:'create_user')
  User createUser;

  Notification(
    this.id,
    this.title,
    this.content,
    this.type,
    this.needSignature,
    this.datePublished,
    this.createUser,
  );

  // @JsonKey(name: 'signature_user')
  //不同的类使用不同的mixin即可
  factory Notification.fromJson(Map<String, dynamic> json) =>
      _$NotificationFromJson(json);
  Map<String, dynamic> toJson() => _$NotificationToJson(this);
}
