// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_history_page.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskHistoryPage _$TaskHistoryPageFromJson(Map<String, dynamic> json) {
  return TaskHistoryPage(
      json['count'] as int,
      (json['results'] as List)
          ?.map((e) => e == null
              ? null
              : TaskHistory.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$TaskHistoryPageToJson(TaskHistoryPage instance) =>
    <String, dynamic>{'count': instance.count, 'results': instance.results};
