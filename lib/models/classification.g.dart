// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'classification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Classification _$ClassificationFromJson(Map<String, dynamic> json) {
  return Classification(json['code'] as String, json['name'] as String);
}

Map<String, dynamic> _$ClassificationToJson(Classification instance) =>
    <String, dynamic>{'code': instance.code, 'name': instance.name};
