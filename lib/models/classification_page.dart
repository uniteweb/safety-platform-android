import 'package:json_annotation/json_annotation.dart';
import 'package:safety_platform/models/classification.dart';


part 'classification_page.g.dart';

@JsonSerializable()
class ClassificationPage {
  int count;
  int next;
  int previous;
  List<Classification> results;

  ClassificationPage(this.count, this.next, this.previous, this.results);


     //不同的类使用不同的mixin即可
  factory ClassificationPage.fromJson(Map<String, dynamic> json) => _$ClassificationPageFromJson(json);
  Map<String, dynamic> toJson() => _$ClassificationPageToJson(this);
}
