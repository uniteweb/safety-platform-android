import 'package:json_annotation/json_annotation.dart';
part 'situation.g.dart';

@JsonSerializable()
class Situation{
  int id;
  String content;
  String improvement;
  @JsonKey(name: 'order_key')
  int orderKey;


  Situation(this.content);

  @override
  String toString(){
    return this.content;
  }

  factory Situation.fromJson(json) => _$SituationFromJson(json);

  Map<String,dynamic> toJson () => _$SituationToJson(this);
}