// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(json['id'] as int, json['screen_name'] as String,
      json['profile'] as String);
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'screen_name': instance.screenName,
      'id': instance.id,
      'profile': instance.profile
    };
