import 'package:json_annotation/json_annotation.dart';
import 'package:safety_platform/models/unit_category.dart';
import 'package:safety_platform/models/risk.dart';
// user.g.dart 将在我们运行生成命令后自动生成
part 'rectify_step.g.dart';

@JsonSerializable()
class RectifyStep {
  int id;

  String title;

  String type;

  String status;

  RectifyStep();

  Map<String,dynamic> toJson() => _$RectifyStepToJson(this);

  factory RectifyStep.fromJson(Map<String,dynamic> json) => _$RectifyStepFromJson(json);
}
