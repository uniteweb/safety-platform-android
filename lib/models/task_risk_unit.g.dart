// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_risk_unit.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskRiskUnit _$TaskRiskUnitFromJson(Map<String, dynamic> json) {
  return TaskRiskUnit(
      json['id'] as int,
      json['unit_code'] as String,
      json['category'] == null
          ? null
          : UnitCategory.fromJson(json['category'] as Map<String, dynamic>),
      json['unit_name'] as String,
      (json['tasks'] as List)
          ?.map((e) =>
              e == null ? null : Task.fromJson(e as Map<String, dynamic>))
          ?.toList(),
      json['cycle_type'] as String,
      json['cycle'] as int,
      json['status'] as String)
    ..created = json['created'] == null
        ? null
        : DateTime.parse(json['created'] as String);
}

Map<String, dynamic> _$TaskRiskUnitToJson(TaskRiskUnit instance) =>
    <String, dynamic>{
      'id': instance.id,
      'unit_code': instance.unitCode,
      'unit_name': instance.unitName,
      'cycle_type': instance.cycleType,
      'category': instance.category,
      'tasks': instance.tasks,
      'cycle': instance.cycle,
      'status': instance.status,
      'created': instance.created?.toIso8601String()
    };
