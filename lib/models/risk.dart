import 'package:json_annotation/json_annotation.dart';
import 'package:safety_platform/models/unit_category.dart';
import 'package:safety_platform/models/accident.dart';
import 'package:safety_platform/models/classification.dart';
import 'package:safety_platform/models/solution.dart';
part 'risk.g.dart';

///这个标注是告诉生成器，这个类是需要生成Model类的

@JsonSerializable()
class RiskFactor {

  RiskFactor();
  String content;

  @JsonKey(name: 'method_of_calculation')
  String methodOfCalculation;

  String grade;

  @JsonKey(name: 'response_unit')
  String responseUnit;


  @JsonKey(name: 'response_person')
  String responsePerson;

  String level;

  @JsonKey(name: 'contact_phone')
  String contactPhone;

  List<Accident> accidents;

  List<Solution> solutions;

  factory RiskFactor.fromJson(json) => _$RiskFactorFromJson(json);

  dynamic toJson() => _$RiskFactorToJson(this);
}

@JsonSerializable()
class Risk {
  int id;
  String name;

  // @JsonKey(name: 'response_person')
  // String responsePerson;
  String remark;
  String level;
  String code;
  List<RiskFactor> factors;
  // String dangers;
  // String standards;
  // String grade;
  // @JsonKey(name: 'method_of_calculation')
  // String methodOfCalculation;
  // @JsonKey(name: 'contact_phone')
  // String contactPhone;
  String status;
  DateTime created;
  // @JsonKey(name: 'response_unit')
  // String responseUnit;
  List<Classification> classification;


  Risk(
      this.id,
      this.name,
     
      this.remark,
      this.level,
      this.code,
      this.status,
      this.created,
    
      this.classification);

  //不同的类使用不同的mixin即可
  factory Risk.fromJson(Map<String, dynamic> json) => _$RiskFromJson(json);
  Map<String, dynamic> toJson() => _$RiskToJson(this);

  static String levelTransform(String level) {
    if (level == null) {
      return level;
    }
    switch (level) {
      case 'large':
        return '重大风险';
      case 'high':
        return '较大风险';
      case 'normal':
        return '一般风险';
      case 'low':
        return '较低风险';
    }
    return level;
  }
}

// "id": 1,
//                     "name": "超声波清洗",
//                     "response_person": "油枪负责1",
//                     "level": "normal",
//                     "contact_phone": "2929299292",
//                     "response_unit": "加油小分队一",
//                     "accidents": [
//                         {
//                             "id": 14,
//                             "code": "014",
//                             "name": "火药爆炸",
//                             "description": null
//                         },
//                         {
//                             "id": 15,
//                             "code": "015",
//                             "name": "瓦斯爆炸",
//                             "description": null
//                         }
//                     ],
//                     "classification": [
//                         {
//                             "code": "200001",
//                             "name": "设备设施"
//                         },
//                         {
//                             "code": "200002",
//                             "name": "特种设备"
//                         }
//                     ],
//                     "code": "TSKY0001",
//                     "dangers": "1. 静电危险。\r\n2.油枪容易失火。\r\n3.油管容易阻塞。\r\n4.不能使用打火机",
//                     "standards": "老子就是标准，老子就是规范",
//                     "remark": "",
//                     "grade": "83",
//                     "method_of_calculation": "",
//                     "status": "valid",
//                     "extension": null,
//                     "created": "2018-12-12T09:26:23.277224",
//                     "updated": "2018-12-12T09:26:23.277224",
//                     "risk_unit": 4
