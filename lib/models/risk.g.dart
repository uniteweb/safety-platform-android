// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'risk.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RiskFactor _$RiskFactorFromJson(Map<String, dynamic> json) {
  return RiskFactor()
    ..content = json['content'] as String
    ..methodOfCalculation = json['method_of_calculation'] as String
    ..grade = json['grade'] as String
    ..responseUnit = json['response_unit'] as String
    ..responsePerson = json['response_person'] as String
    ..level = json['level'] as String
    ..contactPhone = json['contact_phone'] as String
    ..accidents = (json['accidents'] as List)
        ?.map((e) =>
            e == null ? null : Accident.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..solutions = (json['solutions'] as List)
        ?.map((e) =>
            e == null ? null : Solution.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$RiskFactorToJson(RiskFactor instance) =>
    <String, dynamic>{
      'content': instance.content,
      'method_of_calculation': instance.methodOfCalculation,
      'grade': instance.grade,
      'response_unit': instance.responseUnit,
      'response_person': instance.responsePerson,
      'level': instance.level,
      'contact_phone': instance.contactPhone,
      'accidents': instance.accidents,
      'solutions': instance.solutions
    };

Risk _$RiskFromJson(Map<String, dynamic> json) {
  return Risk(
      json['id'] as int,
      json['name'] as String,
      json['remark'] as String,
      json['level'] as String,
      json['code'] as String,
      json['status'] as String,
      json['created'] == null
          ? null
          : DateTime.parse(json['created'] as String),
      (json['classification'] as List)
          ?.map((e) => e == null
              ? null
              : Classification.fromJson(e as Map<String, dynamic>))
          ?.toList())
    ..factors = (json['factors'] as List)
        ?.map((e) => e == null ? null : RiskFactor.fromJson(e))
        ?.toList();
}

Map<String, dynamic> _$RiskToJson(Risk instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'remark': instance.remark,
      'level': instance.level,
      'code': instance.code,
      'factors': instance.factors,
      'status': instance.status,
      'created': instance.created?.toIso8601String(),
      'classification': instance.classification
    };
