import 'package:json_annotation/json_annotation.dart';
import 'dart:convert';
part 'attachment.g.dart';

@JsonSerializable()
class Attachment {
  String id;

  String url;

  String localPath;

  String uploadStatus;

  @JsonKey(name:'content_type')
  String contentType;

  @JsonKey(name: 'screen_name')
  String screenName;

 

  factory Attachment. fromLocal(String localPath, String  screenName ) {
    return Attachment(
      null,
      null,
      localPath,
      'uploading',
      screenName,
    );
  }
  Attachment(
      this.id, this.url, this.localPath, this.uploadStatus, this.screenName);

  //不同的类使用不同的mixin即可
  factory Attachment.fromJson(Map<String, dynamic> json) =>
      _$AttachmentFromJson(json);
  Map<String, dynamic> toJson() => _$AttachmentToJson(this);

  @override
  String toString() {
    return json.encode(this.toJson());
  }
}
