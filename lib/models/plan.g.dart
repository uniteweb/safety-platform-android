// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'plan.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Plan _$PlanFromJson(Map<String, dynamic> json) {
  return Plan(
      json['id'] as int,
      json['status'] as String,
      json['time_of_expiration'] as String,
      json['time_of_start'] as String,
      json['time_of_finish'] as String,
      json['risk_unit'] == null
          ? null
          : RiskUnit.fromJson(json['risk_unit'] as Map<String, dynamic>),
      json['created'] == null
          ? null
          : DateTime.parse(json['created'] as String),
      json['updated'] == null
          ? null
          : DateTime.parse(json['updated'] as String));
}

Map<String, dynamic> _$PlanToJson(Plan instance) => <String, dynamic>{
      'id': instance.id,
      'status': instance.status,
      'time_of_expiration': instance.timeOfExpiration,
      'time_of_start': instance.timeOfStart,
      'time_of_finish': instance.timeOfFinish,
      'created': instance.created?.toIso8601String(),
      'updated': instance.updated?.toIso8601String(),
      'risk_unit': instance.riskUnit
    };
