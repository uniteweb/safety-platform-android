// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Message _$MessageFromJson(Map<String, dynamic> json) {
  return Message()
    ..id = json['id'] as int
    ..content = json['content'] as String
    ..type = json['type'] as String
    ..read = json['read'] as bool
    ..updated = json['updated'] == null
        ? null
        : DateTime.parse(json['updated'] as String)
    ..created = json['created'] == null
        ? null
        : DateTime.parse(json['created'] as String);
}

Map<String, dynamic> _$MessageToJson(Message instance) => <String, dynamic>{
      'id': instance.id,
      'content': instance.content,
      'type': instance.type,
      'read': instance.read,
      'updated': instance.updated?.toIso8601String(),
      'created': instance.created?.toIso8601String()
    };

MessagePage _$MessagePageFromJson(Map<String, dynamic> json) {
  return MessagePage()
    ..count = json['count'] as int
    ..next = json['next'] as int
    ..previous = json['previous'] as int
    ..results = (json['results'] as List)
        ?.map((e) => e == null ? null : Message.fromJson(e))
        ?.toList();
}

Map<String, dynamic> _$MessagePageToJson(MessagePage instance) =>
    <String, dynamic>{
      'count': instance.count,
      'next': instance.next,
      'previous': instance.previous,
      'results': instance.results
    };
