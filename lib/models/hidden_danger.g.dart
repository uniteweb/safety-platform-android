// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hidden_danger.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HiddenDangerRequest _$HiddenDangerRequestFromJson(Map<String, dynamic> json) {
  return HiddenDangerRequest()
    ..memo = json['memo'] as String
    ..riskId = json['risk_id'] as int
    ..status = json['status'] as String
    ..source = json['source'] as String
    ..situationImages = (json['situation_images'] as List)
        ?.map((e) =>
            e == null ? null : Attachment.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..grade = json['grade'] as String
    ..classificationId = json['classification_id'] as String
    ..situations = (json['situations'] as List)
        ?.map((e) => e == null ? null : Situation.fromJson(e))
        ?.toList()
    ..needRetification = json['need_retification'] as bool
    ..traceNo = json['trace_no'] as String;
}

Map<String, dynamic> _$HiddenDangerRequestToJson(
        HiddenDangerRequest instance) =>
    <String, dynamic>{
      'memo': instance.memo,
      'risk_id': instance.riskId,
      'status': instance.status,
      'source': instance.source,
      'situation_images': instance.situationImages,
      'grade': instance.grade,
      'classification_id': instance.classificationId,
      'situations': instance.situations,
      'need_retification': instance.needRetification,
      'trace_no': instance.traceNo
    };

TaskHiddenDangerRequest _$TaskHiddenDangerRequestFromJson(
    Map<String, dynamic> json) {
  return TaskHiddenDangerRequest()
    ..memo = json['memo'] as String
    ..riskId = json['risk_id'] as int
    ..status = json['status'] as String
    ..source = json['source'] as String
    ..situationImages = (json['situation_images'] as List)
        ?.map((e) =>
            e == null ? null : Attachment.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..grade = json['grade'] as String
    ..classificationId = json['classification_id'] as String
    ..situations = (json['situations'] as List)
        ?.map((e) => e == null ? null : Situation.fromJson(e))
        ?.toList()
    ..needRetification = json['need_retification'] as bool
    ..traceNo = json['trace_no'] as String
    ..itemId = json['item_id'] as String;
}

Map<String, dynamic> _$TaskHiddenDangerRequestToJson(
        TaskHiddenDangerRequest instance) =>
    <String, dynamic>{
      'memo': instance.memo,
      'risk_id': instance.riskId,
      'status': instance.status,
      'source': instance.source,
      'situation_images': instance.situationImages,
      'grade': instance.grade,
      'classification_id': instance.classificationId,
      'situations': instance.situations,
      'need_retification': instance.needRetification,
      'trace_no': instance.traceNo,
      'item_id': instance.itemId
    };

HiddenDangerPage _$HiddenDangerPageFromJson(Map<String, dynamic> json) {
  return HiddenDangerPage(
      json['count'] as int,
      json['previous'] as int,
      json['next'] as int,
      (json['results'] as List)
          ?.map((e) => e == null
              ? null
              : HiddenDanger.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$HiddenDangerPageToJson(HiddenDangerPage instance) =>
    <String, dynamic>{
      'count': instance.count,
      'previous': instance.previous,
      'next': instance.next,
      'results': instance.results
    };

HiddenDanger _$HiddenDangerFromJson(Map<String, dynamic> json) {
  return HiddenDanger()
    ..rectifySteps = (json['rectify_steps'] as List)
        ?.map((e) =>
            e == null ? null : RectifyStep.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..id = json['id'] as String
    ..memo = json['memo'] as String
    ..situations = (json['situations'] as List)
        ?.map((e) => e == null ? null : Situation.fromJson(e))
        ?.toList()
    ..attachments = (json['attachments'] as List)
        ?.map((e) =>
            e == null ? null : Attachment.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..classification = json['classification'] == null
        ? null
        : Classification.fromJson(
            json['classification'] as Map<String, dynamic>)
    ..updated = json['updated'] == null
        ? null
        : DateTime.parse(json['updated'] as String)
    ..situationImages = (json['situation_images'] as List)
        ?.map((e) =>
            e == null ? null : Attachment.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..verifiedImages = (json['verified_images'] as List)
        ?.map((e) =>
            e == null ? null : Attachment.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..created = json['created'] == null
        ? null
        : DateTime.parse(json['created'] as String)
    ..status = json['status'] as String
    ..source = json['source'] as String
    ..retifyDate = json['retify_date'] == null
        ? null
        : DateTime.parse(json['retify_date'] as String)
    ..needRetification = json['need_retification'] as bool
    ..traceNo = json['trace_no'] as String
    ..grade = json['grade'] as String
    ..risk = json['risk'] == null
        ? null
        : Risk.fromJson(json['risk'] as Map<String, dynamic>)
    ..rectificationCost = json['rectification_cost'] as String
    ..rectificationBudget = json['rectification_budget'] as String
    ..createUser = json['user_of_created'] == null
        ? null
        : User.fromJson(json['user_of_created'] as Map<String, dynamic>)
    ..approveUser = json['user_of_approved'] == null
        ? null
        : User.fromJson(json['user_of_approved'] as Map<String, dynamic>);
}

Map<String, dynamic> _$HiddenDangerToJson(HiddenDanger instance) =>
    <String, dynamic>{
      'rectify_steps': instance.rectifySteps,
      'id': instance.id,
      'memo': instance.memo,
      'situations': instance.situations,
      'attachments': instance.attachments,
      'classification': instance.classification,
      'updated': instance.updated?.toIso8601String(),
      'situation_images': instance.situationImages,
      'verified_images': instance.verifiedImages,
      'created': instance.created?.toIso8601String(),
      'status': instance.status,
      'source': instance.source,
      'retify_date': instance.retifyDate?.toIso8601String(),
      'need_retification': instance.needRetification,
      'trace_no': instance.traceNo,
      'grade': instance.grade,
      'risk': instance.risk,
      'rectification_cost': instance.rectificationCost,
      'rectification_budget': instance.rectificationBudget,
      'user_of_created': instance.createUser,
      'user_of_approved': instance.approveUser
    };
