// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'attachment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Attachment _$AttachmentFromJson(Map<String, dynamic> json) {
  return Attachment(
      json['id'] as String,
      json['url'] as String,
      json['localPath'] as String,
      json['uploadStatus'] as String,
      json['screen_name'] as String)
    ..contentType = json['content_type'] as String;
}

Map<String, dynamic> _$AttachmentToJson(Attachment instance) =>
    <String, dynamic>{
      'id': instance.id,
      'url': instance.url,
      'localPath': instance.localPath,
      'uploadStatus': instance.uploadStatus,
      'content_type': instance.contentType,
      'screen_name': instance.screenName
    };
