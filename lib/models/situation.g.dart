// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'situation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Situation _$SituationFromJson(Map<String, dynamic> json) {
  return Situation(json['content'] as String)
    ..id = json['id'] as int
    ..improvement = json['improvement'] as String
    ..orderKey = json['order_key'] as int;
}

Map<String, dynamic> _$SituationToJson(Situation instance) => <String, dynamic>{
      'id': instance.id,
      'content': instance.content,
      'improvement': instance.improvement,
      'order_key': instance.orderKey
    };
