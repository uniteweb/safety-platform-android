import 'package:json_annotation/json_annotation.dart';
import 'package:safety_platform/models/unit_category.dart';

part 'classification.g.dart';

///这个标注是告诉生成器，这个类是需要生成Model类的
@JsonSerializable()
class Classification{

  String code;
  String name;

  Classification(this.code,this.name);


   //不同的类使用不同的mixin即可
  factory Classification.fromJson(Map<String, dynamic> json) => _$ClassificationFromJson(json);
  Map<String, dynamic> toJson() => _$ClassificationToJson(this);
  
  @override
    String toString() {
      // TODO: implement toString
      return this.name;
    }
}