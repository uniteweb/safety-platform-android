import 'package:json_annotation/json_annotation.dart';
import 'package:safety_platform/models/notification.dart';
part 'received_notification.g.dart';

@JsonSerializable()
class ReceivedNotification {
  int id;
  @JsonKey(name: 'signature_time')
  DateTime signatureTime;

  @JsonKey(name: 'read_time')
  DateTime readTime;

  DateTime updated;

  DateTime created;

  Notification notification;

  String status;

  bool read;

  bool signed;

  ReceivedNotification(this.id, this.signatureTime, this.readTime, this.updated,
      this.status, this.read, this.signed, this.created, this.notification);


  String  toString() {
    return toJson().toString();

  }
  // @JsonKey(name: 'signature_user')

  // @JsonKey(name: 'signature_user')
  //不同的类使用不同的mixin即可
  factory ReceivedNotification.fromJson(Map<String, dynamic> json) =>
      _$ReceivedNotificationFromJson(json);
  Map<String, dynamic> toJson() => _$ReceivedNotificationToJson(this);
}
