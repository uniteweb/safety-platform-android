// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'task_page.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TaskPage _$TaskPageFromJson(Map<String, dynamic> json) {
  return TaskPage(
      json['count'] as int, json['next'] as String, json['previous'] as String)
    ..results = (json['results'] as List)
        ?.map((e) =>
            e == null ? null : TaskRiskUnit.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$TaskPageToJson(TaskPage instance) => <String, dynamic>{
      'count': instance.count,
      'next': instance.next,
      'previous': instance.previous,
      'results': instance.results
    };
