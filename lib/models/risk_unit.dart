import 'package:json_annotation/json_annotation.dart';
import 'package:safety_platform/models/unit_category.dart';
import 'package:safety_platform/models/risk.dart';
// user.g.dart 将在我们运行生成命令后自动生成
part 'risk_unit.g.dart';

///这个标注是告诉生成器，这个类是需要生成Model类的

@JsonSerializable()
class RiskUnitPage {
  int count;
  int next;
  int previous;
  List<RiskUnit> results;

  RiskUnitPage();

  factory RiskUnitPage.fromJson(Map<String,dynamic> json) => _$RiskUnitPageFromJson(json);
}

@JsonSerializable()
class RiskUnit {
  RiskUnit(this.id, this.unitCode, this.unitName, this.category, this.cycleType,
      this.cycle, this.status, this.risks);
  int id;
  @JsonKey(name: 'unit_code')
  String unitCode;

  @JsonKey(name: 'unit_name')
  String unitName;

  @JsonKey(name: 'cycle_type')
  String cycleType;

  UnitCategory category;

  List<Risk> risks;

  @JsonKey(name:'risk_count')
  int riskCount;

  int cycle;

  String status;

  DateTime created;

  //不同的类使用不同的mixin即可
  factory RiskUnit.fromJson(Map<String, dynamic> json) =>
      _$RiskUnitFromJson(json);
  Map<String, dynamic> toJson() => _$RiskUnitToJson(this);

  @override
  String toString() {
    // TODO: implement toString
    return 'id:${this.id}, unitCode:${this.unitCode}, unitName:${this.unitName}';
  }
}
