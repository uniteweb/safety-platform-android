import 'package:json_annotation/json_annotation.dart';
import 'task_risk_unit.dart';
// user.g.dart 将在我们运行生成命令后自动生成
part 'task_page.g.dart';

///这个标注是告诉生成器，这个类是需要生成Model类的
@JsonSerializable()
class TaskPage extends Object {
  int count;
  String next;
  String previous;

  List<TaskRiskUnit> results;

  TaskPage(this.count, this.next, this.previous);


  static String transStatus(String status) {
    switch(status) {
      case 'pending': return '未检查';
      case 'done': return '已完成';
      case 'risk': return '有风险';
    }
    return status;

  }
  //不同的类使用不同的mixin即可
  factory TaskPage.fromJson(Map<String, dynamic> json) =>
      _$TaskPageFromJson(json);
  Map<String, dynamic> toJson() => _$TaskPageToJson(this);

  @override
  String toString() {
    return 'count:${this.count}, previous:${this.previous},next:${this.next},results:${this.results}';
  }
}
