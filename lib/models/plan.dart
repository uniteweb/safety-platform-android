import 'package:json_annotation/json_annotation.dart';
import 'package:safety_platform/models/risk_unit.dart';
// user.g.dart 将在我们运行生成命令后自动生成
part 'plan.g.dart';

///这个标注是告诉生成器，这个类是需要生成Model类的
@JsonSerializable()
class Plan {
  int id;
  String status;
  @JsonKey(name: 'time_of_expiration')
  String timeOfExpiration;

  @JsonKey(name: 'time_of_start')
  String timeOfStart;

  @JsonKey(name: 'time_of_finish')
  String timeOfFinish;

  DateTime created;

  DateTime updated;

  @JsonKey(name: 'risk_unit')
  RiskUnit  riskUnit;
  

  Plan(this.id, this.status, this.timeOfExpiration, this.timeOfStart,
      this.timeOfFinish, this.riskUnit, this.created, this.updated);

  //不同的类使用不同的mixin即可
  factory Plan.fromJson(Map<String, dynamic> json) => _$PlanFromJson(json);
  Map<String, dynamic> toJson() => _$PlanToJson(this);

  @override
  String toString() {
    // TODO: implement toString
    return 'id:${this.id}, status:${status}, timeOfExpiration: ${timeOfExpiration}, timeOfStart: ${timeOfStart}, timeOfFinish：${timeOfFinish}, riskUnit: ${riskUnit},created: ${created}, updated: ${updated}';
  }
}
