// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Notification _$NotificationFromJson(Map<String, dynamic> json) {
  return Notification(
      json['id'] as int,
      json['title'] as String,
      json['content'] as String,
      json['type'] as String,
      json['need_signature'] as bool,
      json['date_published'] == null
          ? null
          : DateTime.parse(json['date_published'] as String),
      json['create_user'] == null
          ? null
          : User.fromJson(json['create_user'] as Map<String, dynamic>));
}

Map<String, dynamic> _$NotificationToJson(Notification instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'content': instance.content,
      'type': instance.type,
      'need_signature': instance.needSignature,
      'date_published': instance.datePublished?.toIso8601String(),
      'create_user': instance.createUser
    };
