import 'package:json_annotation/json_annotation.dart';
import 'package:safety_platform/models/classification.dart';
import 'package:safety_platform/models/attachment.dart';
import 'package:safety_platform/models/rectify_step.dart';
import 'package:safety_platform/models/risk.dart';
import 'package:safety_platform/models/user.dart';
import 'package:safety_platform/models/situation.dart';
part 'hidden_danger.g.dart';

String hiddenDangerStatusFromCode(String code) {
  switch (code) {
    case 'created':
      return '已登记';
    case 'approved':
      return '已安排';
    case 'done':
      return '已完成';
    case 'verified':
      return '已验收';
  }
  return code;
}

String hiddenDangerGradeCodeToText(String code) {
  switch (code) {
    case 'normal':
      return '一般事故隐患';
    case 'low':
      return '轻微事故隐患';
    case 'large':
      return '重大事故隐患';
  }
  return code;
}

String hiddenDangerSourceFormCode(String code) {
  switch (code) {
    case 'C':
      return '企业自查';
  }
  return code;
}

String hiddenDangerGradeToCode(String title) {
  switch (title) {
    case '一般事故隐患':
      {
        return 'normal';
      }
    case '重大事故隐患':
      {
        return 'large';
      }
    case '轻微事故隐患':
      {
        return 'low';
      }
    // return title;
  }

  return title;
}

@JsonSerializable()
class HiddenDangerRequest {
  String memo;

  @JsonKey(name: 'risk_id')
  int riskId;

  String status;

  String source;

  @JsonKey(name: 'situation_images')
  List<Attachment> situationImages;

  String grade;



  @JsonKey(name: 'classification_id')
  String classificationId;
  // Classification classification;
  List<Situation> situations;
  
  @JsonKey(name: 'need_retification')
  bool needRetification;

  @JsonKey(name: 'trace_no')
  String traceNo;

  HiddenDangerRequest() {}

  factory HiddenDangerRequest.fromJson(Map<String, dynamic> json) =>
      _$HiddenDangerRequestFromJson(json);

  Map<String, dynamic> toJson() => _$HiddenDangerRequestToJson(this);
  //  factory HiddenDangerRequest.fromJson(Map<String, dynamic> json) =>
  //     _$HiddenDangerRequestFromJson(json);
  // Map<String, dynamic> toJson() => _$NotificationToJson(this);
}

@JsonSerializable()
class TaskHiddenDangerRequest extends HiddenDangerRequest {
  @JsonKey(name: 'item_id')
  String itemId;
  TaskHiddenDangerRequest();
  factory TaskHiddenDangerRequest.copyForm(HiddenDangerRequest hr) {
    TaskHiddenDangerRequest request = TaskHiddenDangerRequest();
    request.memo = hr.memo;
    request.riskId = hr.riskId;
    request.source = hr.source;
    request.situationImages = hr.situationImages;
    request.grade = hr.grade;
    request.status = hr.status;
    request.classificationId = hr.classificationId;
    request.needRetification = hr.needRetification;
    request.traceNo = hr.traceNo;

    return request;
  }

  Map<String, dynamic> toJson() => _$TaskHiddenDangerRequestToJson(this);
}

@JsonSerializable()
class HiddenDangerPage {
  int count;
  int previous;
  int next;
  List<HiddenDanger> results;

  HiddenDangerPage(this.count, this.previous, this.next, this.results);

  factory HiddenDangerPage.fromJson(Map<String, dynamic> json) =>
      _$HiddenDangerPageFromJson(json);

  Map<String, dynamic> toJson() => _$HiddenDangerPageToJson(this);
}

@JsonSerializable()
class HiddenDanger {
  factory HiddenDanger.fromJson(Map<String, dynamic> json) =>
      _$HiddenDangerFromJson(json);
  @JsonKey(name: 'rectify_steps')
  List<RectifyStep> rectifySteps;
  Map<String, dynamic> toJson() => _$HiddenDangerToJson(this);
  HiddenDanger() {}
  String id;

  String memo;

  List<Situation> situations;

  List<Attachment> attachments;

  Classification classification;

  DateTime updated;

    @JsonKey(name: 'situation_images')
  List<Attachment> situationImages;

  @JsonKey(name: 'verified_images')
  List<Attachment> verifiedImages;

  DateTime created;

  String status;

  String source;
  @JsonKey(name: 'retify_date')
  DateTime retifyDate;

  @JsonKey(name: 'need_retification')
  bool needRetification;

  @JsonKey(name: 'trace_no')
  String traceNo;

  String grade;

  Risk risk;

  @JsonKey(name: 'rectification_cost')
  String rectificationCost;

  @JsonKey(name: 'rectification_budget')
  String rectificationBudget;

  @JsonKey(name: 'user_of_created')
  User createUser;

  @JsonKey(name: 'user_of_approved')
  User approveUser;
}
