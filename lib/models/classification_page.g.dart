// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'classification_page.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ClassificationPage _$ClassificationPageFromJson(Map<String, dynamic> json) {
  return ClassificationPage(
      json['count'] as int,
      json['next'] as int,
      json['previous'] as int,
      (json['results'] as List)
          ?.map((e) => e == null
              ? null
              : Classification.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$ClassificationPageToJson(ClassificationPage instance) =>
    <String, dynamic>{
      'count': instance.count,
      'next': instance.next,
      'previous': instance.previous,
      'results': instance.results
    };
