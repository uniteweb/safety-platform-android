import 'package:safety_platform/models/task.dart';
import 'package:equatable/equatable.dart';

class TaskListState extends Equatable {
  TaskListState({this.totalCount: 0, this.nextPage, this.results = const []})
      : super([totalCount, nextPage, results]);

  int totalCount;

  String nextPage;

  List<Task> results;

  @override
  String toString() {
    return ('TaskListState[${runtimeType.toString()}]: ${results.length}');
  }
}

class TaskListLoadingState extends TaskListState {}


class TaskDetailState{
  TaskDetailState(this.task);
  Task task;
}

