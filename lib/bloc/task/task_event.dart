import 'package:safety_platform/models/task.dart';
import 'package:safety_platform/models/attachment.dart';

class TaskListEvent {}

class QueryTodayTaskListEvent extends TaskListEvent {
  QueryTodayTaskListEvent({this.throwLoadingState: true, this.page});
  int page = 1;

  bool throwLoadingState = true;
}

class TaskEvent {}

class GetTaskItemsEvent extends TaskEvent {
  GetTaskItemsEvent(this.taskId);
  String taskId;
}

class UploadAttachmentEvent extends TaskEvent {
  TaskItem item;
  Attachment attachment;
  UploadAttachmentEvent(this.item, this.attachment);
}

class SubmitTaskEvent extends TaskEvent {
  SubmitTaskEvent(this.task);
  Task task;
}
class TaskItemStatueChangedEvent extends TaskEvent {
  TaskItemStatueChangedEvent(this.item,{ this.answer , this.remark, this.attachments});
  TaskItem item;

  String answer;

  String remark;

  List<Attachment> attachments;
}

class CleanTaskItemStatusEvent extends TaskItemStatueChangedEvent {
  CleanTaskItemStatusEvent(TaskItem item):super(item, answer:null,remark:null, attachments:null);
}
