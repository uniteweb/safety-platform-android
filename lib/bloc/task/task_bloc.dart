import './task_event.dart';
import './task_state.dart';
import 'package:bloc/bloc.dart';
import 'package:safety_platform/repository/task.dart';
import 'package:safety_platform/repository/attachment.dart';
import 'package:safety_platform/models/task.dart';
import 'package:safety_platform/models/attachment.dart';

class TaskBloc extends Bloc<TaskEvent, TaskDetailState> {
  TaskDetailState inital;

  TaskBloc({this.inital});

  final TaskRepository _repository = TaskRepository();

  final AttachmentRepository _attachRepository = AttachmentRepository();

  @override
  TaskDetailState get initialState => inital;

  TaskItem _findTaskItem(Task task, String itemId) {
    for (TaskItem i in task.items) {
      if (i.id == itemId) {
        return i;
      }
    }
    return null;
  }

  @override
  Stream<TaskDetailState> mapEventToState(
      TaskDetailState currentState, TaskEvent event) async* {

    if (event is SubmitTaskEvent) {
      Task task = await _repository.submitTask(event.task);
      yield TaskDetailState(task);
    }
    else if (event is GetTaskItemsEvent) {
      String taskId = event.taskId;

      Task task = await _repository.getTaskDetail(taskId);

      yield TaskDetailState(task);
    } else if (event is CleanTaskItemStatusEvent) {
      TaskItem item =_findTaskItem(currentState.task, event.item.id);
      if (item !=null) {
        item.remark = null;
        item.result = null;
        item.attachments = null;

        yield TaskDetailState(currentState.task);
      }
    } else if (event is TaskItemStatueChangedEvent) {
      Task task = currentState.task;

      for (TaskItem i in task.items) {
        if (i.id == event.item.id) {
          if (event.answer != null) {
            i.result = event.answer;
          }
          if (event.remark != null) {
            i.remark = event.remark;
          }
        }
      }
      yield TaskDetailState(task);
    } else if (event is UploadAttachmentEvent) {
      Task task = currentState.task;

      TaskItem taskItem;
      for (TaskItem i in task.items) {
        if (i.id == event.item.id) {
          taskItem = i;
        }
      }
      if (taskItem != null) {
        Attachment attachment = event.attachment;
        if (taskItem.attachments == null) {
          taskItem.attachments = [];
        }
        taskItem.attachments.add(attachment);

        yield TaskDetailState(task);

        Attachment uploadedAttach = await _attachRepository.storeAttachment(
            'inspection.TaskItemImage', attachment);
        attachment.id = uploadedAttach.id;
        attachment.url = uploadedAttach.url;
        attachment.uploadStatus = 'done';
        yield TaskDetailState(task);
      }
    }
  }
}

class TaskListBloc extends Bloc<TaskListEvent, TaskListState> {
  final TaskRepository _repository = TaskRepository();
  @override
  TaskListState get initialState => TaskListState();

  @override
  Stream<TaskListState> mapEventToState(
      TaskListState currentState, TaskListEvent event) async* {
    if (event is QueryTodayTaskListEvent) {
      if (event.throwLoadingState) {
        yield TaskListLoadingState();
      }
      TaskPage page = await _repository.loadTodayTask(page: event.page);
      print('page-> ${page.toJson()}');
      yield TaskListState(
          totalCount: page.count, nextPage: page.next, results: page.results);
    }
  }
}
