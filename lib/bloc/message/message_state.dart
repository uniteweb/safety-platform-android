import 'package:equatable/equatable.dart';
import 'package:safety_platform/models/message.dart';

class MessageState extends Equatable {
  MessageState([options]) : super([options]);
}

class MessageUpdatedState extends MessageState {
  List<Message> messages;
  int totalCount;
  int nextPage;

  MessageUpdatedState(
    this.totalCount,
    this.nextPage,
    this.messages) : super([messages,totalCount,nextPage]);
}
