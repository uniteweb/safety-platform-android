import 'package:equatable/equatable.dart';
import 'package:safety_platform/models/message.dart';

abstract class MessageEvent extends Equatable {
  MessageEvent([List props = const []]) : super(props);
}

class MessageRefreshEvent extends MessageEvent {
  // List<String> messages  ;

 
  // MessageRefreshEvent(this.totalCount, this.nextPage, this.message)
  //     : super([totalCount, nextPage, message]);
}
