import 'package:bloc/bloc.dart';
import './message_event.dart';
import './message_state.dart';
import 'package:safety_platform/pages/services.dart';
import 'package:safety_platform/models/message.dart';
import 'package:safety_platform/repository/message.dart';

class MessageBloc extends Bloc<MessageEvent, MessageState> {
  @override
  MessageState get initialState =>
      MessageState(); // TODO: implement initialState;

  MessageRepository _repository = MessageRepository();

  @override
  Stream<MessageState> mapEventToState(
      MessageState currentState, MessageEvent event) async* {

    if (event is MessageRefreshEvent) {
       try {
      MessagePage messagePage = await _repository.loadMessages();

      yield MessageUpdatedState(
          messagePage.count, messagePage.next, messagePage.results);
    } on ServiceException catch (e) { 
    }
  }

  _messageRefresh(MessageRefreshEvent event) async* {
   }
  }
}
