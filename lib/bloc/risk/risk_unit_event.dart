import 'package:equatable/equatable.dart';
import 'package:safety_platform/models/risk_unit.dart';
class RiskUnitEvent extends Equatable {

}

class RiskUnitRefreshEvent extends RiskUnitEvent {
  bool throwLoadingState = false;
}




class RiskUnitDetailEvent extends Equatable{
  
}

class RiskUnitStartupEvent extends RiskUnitDetailEvent{
  String riskUnitId;

  RiskUnit riskUnit;

  RiskUnitStartupEvent({this.riskUnitId,this.riskUnit});
}

