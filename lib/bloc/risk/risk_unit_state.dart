
import 'package:equatable/equatable.dart';
import 'package:safety_platform/models/risk_unit.dart';
class RiskUnitListState extends Equatable {

  RiskUnitListState(this.data):super([data]);
  List<RiskUnit> data;

}

class RiskUnitDetailState extends Equatable {

  RiskUnit riskUnit;
  RiskUnitDetailState(this.riskUnit): super([riskUnit]);
}
class EmptyRiskUnitDetailState extends RiskUnitDetailState {
  EmptyRiskUnitDetailState():super(null);
}
class LoadingRiskUnitDetailState extends RiskUnitDetailState {
  LoadingRiskUnitDetailState(RiskUnit riskUnit): super(riskUnit);
}

class LoadingRiskUnitRiskState extends RiskUnitDetailState{
  LoadingRiskUnitRiskState(RiskUnit riskUnit): super(riskUnit);
}