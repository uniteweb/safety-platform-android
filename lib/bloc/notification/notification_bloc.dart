import 'package:bloc/bloc.dart';
import './notification_event.dart';
import './notification_state.dart';
import 'dart:async';
import 'package:safety_platform/repository/notification.dart';
import 'package:safety_platform/models/received_notification_page.dart';
import 'package:safety_platform/models/received_notification.dart';

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  final NotificationRepository _repository = NotificationRepository();
  @override
  NotificationState get initialState =>
      NotificationLoadingState(); // TODO: implement initialState;

  Future<NotificationDataUpdated> t() async =>
      Future.value(NotificationDataUpdated());
  @override
  Stream<NotificationState> mapEventToState(
      NotificationState currentState, NotificationEvent event) async* {
    if (event is NotificationStartupEvent) {
      NotificationState state = NotificationState();
      await state.formPersist();
      yield state;
      // yield state;

      this.dispatch(NotficationRefreshEvent(throwLoadState: false));

      //  ReceivedNotificationPage page = await _repository
      // .loadReceivedNotification(page: 1, pageSize: 6);
      // NotificationState newState =  state.copyWith(data:page.results, totalCount:page.count, nextPage: page.next);
      // yield newState;
      // await state.persist(newState);

    } else if (event is NotficationRefreshEvent) {
      if (event.throwLoadState) {
        yield NotificationLoadingState();
      }
      ReceivedNotificationPage page = await _repository
          .loadReceivedNotification(page: event.page, pageSize: 6);

      if (page == null || page.count == null || page.count <= 0) {
        yield NotificationDataIsEmpty();
      } else {
        NotificationDataUpdated state = NotificationDataUpdated();

        if (event.loadMore) {
          NotificationDataUpdated currenDataState =
              currentState as NotificationDataUpdated;

          state.data = currenDataState.data;
          state.data.addAll(page.results);
          state.totalCount = page.count;
          state.nextPage = page.next;
        } else {
          state.data = page.results;
          state.totalCount = page.count;
          state.nextPage = page.next;
          await state.persist();
        } 
        yield state;
      }
    } else if (event is NotificationUpdatedEvent) {
      ReceivedNotification rn = event.notification;

      List<ReceivedNotification> data = currentState.data;

      List<ReceivedNotification> newData = data.map((r) {
        if (rn.id == r.id) {
          r.read = rn.read;
          r.signed = rn.signed;
          r.updated = rn.updated;
        }
        return r;
      }).toList();

      yield currentState.copyWith(data: newData);
    }
  }

  // // StreamController _waitStream = StreamController();
  // Future<Null> dispatchAndWait(NofiticationEvent event) async {
  //   this.dispatch(event);
  //   print('before listen');
  //   this.state.listen((_) => Future.value(null));
  //   print('after listen');
  // }
}

class NotificationOperationBloc
    extends Bloc<NotificationOperationEvent, NotificationOperationState> {
  final NotificationRepository _repository = NotificationRepository();

  NotificationOperationState initial;
  NotificationOperationBloc({this.initial});
  @override
  NotificationOperationState get initialState => initial;

  @override
  Stream<NotificationOperationState> mapEventToState(
      NotificationOperationState currentState,
      NotificationOperationEvent event) async* {
    print('收到事件：########## $event');
    if (event is ReadEvent) {
      ReceivedNotification rn =
          await _repository.markNotificationRead(event.notification.id);

      yield NotificationOperationState(rn);
    } else if (event is SignEvent) {
      yield currentState.copyWith(submiiting: true);

      ReceivedNotification rn =
          await _repository.markNotificationSigned(event.notification.id);
      yield SubmittingSuccessState(rn);
    }
    // yield NotificationOperationState(currentState.data, submitting: true);
  }
}
