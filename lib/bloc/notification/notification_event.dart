import 'package:equatable/equatable.dart';
import 'package:safety_platform/models/received_notification.dart';

class NotificationEvent extends Equatable {}

class NotificationStartupEvent extends NotificationEvent {}
class NotficationRefreshEvent extends NotificationEvent {
  final bool loadMore;

  final int page;

  final int pageSize;

  final bool throwLoadState;

  NotficationRefreshEvent(
      {this.loadMore: false,
      this.page: 1,
      this.pageSize,
      this.throwLoadState: true});

  @override
  String toString() {
    return 'NotficationRefreshEvent';
  }
}

class NotificationUpdatedEvent extends NotificationEvent {
  ReceivedNotification notification;

  NotificationUpdatedEvent(this.notification);
}

class NotificationOperationEvent extends Equatable {}

class ReadEvent extends NotificationOperationEvent {
  ReceivedNotification notification;

  ReadEvent(this.notification);
}

class SignEvent extends NotificationOperationEvent {
  ReceivedNotification notification;

  SignEvent(this.notification);
}
