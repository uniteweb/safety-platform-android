import 'package:flutter/widgets.dart';


// 代码由程序自动生成。
// 请不要对此文件做任何修改。详细说明见本文件末尾


class MyIconFont {

    MyIconFont._();

    static const font_name = 'MyIconFont';

    static const IconData ava_error = const IconData(0xe629, fontFamily: font_name);
    static const IconData beizhu_ = const IconData(0xe60b, fontFamily: font_name);
    static const IconData bianpao = const IconData(0xe604, fontFamily: font_name);
    static const IconData chakanyanjingshishifenxi = const IconData(0xe7d3, fontFamily: font_name);
    static const IconData daohang = const IconData(0xe60a, fontFamily: font_name);
    static const IconData dingdan = const IconData(0xe608, fontFamily: font_name);
    static const IconData dingdan_xuanzhong = const IconData(0xe61d, fontFamily: font_name);
    static const IconData dingwei = const IconData(0xe699, fontFamily: font_name);
    static const IconData dingwei1 = const IconData(0xe607, fontFamily: font_name);
    static const IconData dingwei2 = const IconData(0xe647, fontFamily: font_name);
    static const IconData ditu2 = const IconData(0xe600, fontFamily: font_name);
    static const IconData dkw_luyin = const IconData(0xe603, fontFamily: font_name);
    static const IconData done_copy = const IconData(0xe87b, fontFamily: font_name);
    static const IconData excel = const IconData(0xe628, fontFamily: font_name);
    static const IconData fenchen = const IconData(0xe614, fontFamily: font_name);
    static const IconData fengxianyujing = const IconData(0xe609, fontFamily: font_name);
    static const IconData fenxiang = const IconData(0xe659, fontFamily: font_name);
    static const IconData forward = const IconData(0xe68b, fontFamily: font_name);
    static const IconData gif = const IconData(0xe63c, fontFamily: font_name);
    static const IconData guolv = const IconData(0xe64c, fontFamily: font_name);
    static const IconData iconfontdianhua3 = const IconData(0xe611, fontFamily: font_name);
    static const IconData iconqyxx = const IconData(0xe61a, fontFamily: font_name);
    static const IconData icotianping = const IconData(0xe62d, fontFamily: font_name);
    static const IconData jia = const IconData(0xe623, fontFamily: font_name);
    static const IconData jianchaxiang = const IconData(0xe65a, fontFamily: font_name);
    static const IconData jiantou_zuo = const IconData(0xe617, fontFamily: font_name);
    static const IconData jinggao = const IconData(0xe651, fontFamily: font_name);
    static const IconData jpg = const IconData(0xe638, fontFamily: font_name);
    static const IconData jspjiancha = const IconData(0xe631, fontFamily: font_name);
    static const IconData kaishixuncha = const IconData(0xe612, fontFamily: font_name);
    static const IconData lajitong = const IconData(0xe62b, fontFamily: font_name);
    static const IconData localoffer = const IconData(0xe61e, fontFamily: font_name);
    static const IconData meiyoushuju = const IconData(0xe648, fontFamily: font_name);
    static const IconData meiyoushujutongji = const IconData(0xe6b8, fontFamily: font_name);
    static const IconData mp3 = const IconData(0xe639, fontFamily: font_name);
    static const IconData no_data = const IconData(0xe60f, fontFamily: font_name);
    static const IconData nodata = const IconData(0xe6fe, fontFamily: font_name);
    static const IconData note = const IconData(0xe6ca, fontFamily: font_name);
    static const IconData password = const IconData(0xe62c, fontFamily: font_name);
    static const IconData pdf = const IconData(0xe627, fontFamily: font_name);
    static const IconData phone = const IconData(0xe637, fontFamily: font_name);
    static const IconData pingjia = const IconData(0xe620, fontFamily: font_name);
    static const IconData png = const IconData(0xe62f, fontFamily: font_name);
    static const IconData ppt = const IconData(0xe633, fontFamily: font_name);
    static const IconData qiye = const IconData(0xe728, fontFamily: font_name);
    static const IconData reset = const IconData(0xe619, fontFamily: font_name);
    static const IconData riqi = const IconData(0xe613, fontFamily: font_name);
    static const IconData scan = const IconData(0xe6c7, fontFamily: font_name);
    static const IconData shandian_shi = const IconData(0xe65c, fontFamily: font_name);
    static const IconData shandianjisu = const IconData(0xe64e, fontFamily: font_name);
    static const IconData shipin = const IconData(0xe618, fontFamily: font_name);
    static const IconData shoucang = const IconData(0xe65d, fontFamily: font_name);
    static const IconData shu = const IconData(0xe729, fontFamily: font_name);
    static const IconData shu2 = const IconData(0xe731, fontFamily: font_name);
    static const IconData shuji2 = const IconData(0xe816, fontFamily: font_name);
    static const IconData sort_icon = const IconData(0xe668, fontFamily: font_name);
    static const IconData sousuo = const IconData(0xe7a8, fontFamily: font_name);
    static const IconData suoyouweixianyuan = const IconData(0xe6c1, fontFamily: font_name);
    static const IconData sycolor_ = const IconData(0xe78b, fontFamily: font_name);
    static const IconData tongzhi = const IconData(0xe61c, fontFamily: font_name);
    static const IconData tubiao313 = const IconData(0xe624, fontFamily: font_name);
    static const IconData tubiao314 = const IconData(0xe646, fontFamily: font_name);
    static const IconData tupian01 = const IconData(0xe87a, fontFamily: font_name);
    static const IconData txt = const IconData(0xe630, fontFamily: font_name);
    static const IconData unknow = const IconData(0xe634, fontFamily: font_name);
    static const IconData update = const IconData(0xe71a, fontFamily: font_name);
    static const IconData uploaderror = const IconData(0xe674, fontFamily: font_name);
    static const IconData uploadsuccess = const IconData(0xe679, fontFamily: font_name);
    static const IconData versionupdaterule_copy = const IconData(0xe621, fontFamily: font_name);
    static const IconData wancheng = const IconData(0xe62a, fontFamily: font_name);
    static const IconData wancheng1 = const IconData(0xe616, fontFamily: font_name);
    static const IconData wancheng2 = const IconData(0xe6a3, fontFamily: font_name);
    static const IconData warning = const IconData(0xe60c, fontFamily: font_name);
    static const IconData web_icon_ = const IconData(0xe6cd, fontFamily: font_name);
    static const IconData web_icon_1 = const IconData(0xe6f0, fontFamily: font_name);
    static const IconData weibiaoti526 = const IconData(0xe61f, fontFamily: font_name);
    static const IconData weixianhuaxuepinshengchanchucunshiyongqiyeanquanpingjiabao = const IconData(0xe64a, fontFamily: font_name);
    static const IconData weizhi = const IconData(0xe737, fontFamily: font_name);
    static const IconData wenti = const IconData(0xe689, fontFamily: font_name);
    static const IconData wode = const IconData(0xe606, fontFamily: font_name);
    static const IconData wode1 = const IconData(0xe60e, fontFamily: font_name);
    static const IconData wode_wode = const IconData(0xe60d, fontFamily: font_name);
    static const IconData wode_xuanzhong = const IconData(0xe61b, fontFamily: font_name);
    static const IconData word = const IconData(0xe626, fontFamily: font_name);
    static const IconData xiangji = const IconData(0xe610, fontFamily: font_name);
    static const IconData xiangji1fill = const IconData(0xe77e, fontFamily: font_name);
    static const IconData xiangqing = const IconData(0xe632, fontFamily: font_name);
    static const IconData xingzhengzhifa = const IconData(0xe636, fontFamily: font_name);
    static const IconData xinxi = const IconData(0xe622, fontFamily: font_name);
    static const IconData yiliaofangyu = const IconData(0xe615, fontFamily: font_name);
    static const IconData yinhuanjiancha = const IconData(0xe601, fontFamily: font_name);
    static const IconData yonghuming = const IconData(0xe801, fontFamily: font_name);
    static const IconData zhenggaijiancha = const IconData(0xe62e, fontFamily: font_name);
    static const IconData zhibiaozhushibiaozhu = const IconData(0xe7b4, fontFamily: font_name);
    static const IconData zip = const IconData(0xe635, fontFamily: font_name);

/*
如果你有新的图标需要导入到字体中：

1. 联系我，将你加入到 iconfont(http://www.iconfont.cn) 的 xxx 项目中
2. 在 iconfont 中选择需要的图标，加入购物车
3. 然后点击顶部的购物车，弹出右边栏，选择“添加到项目”，选择 xxx，点击“确定”
4. 最好修改一下每个新增图标的名称，然后下载至本地
5. 将压缩包内所有文件拷贝到 iconfont 目录下，形如：
iconfont
├── demo.css
├── demo_fontclass.html
├── demo_symbol.html
├── demo_unicode.html
├── iconfont.css
├── iconfont.eot
├── iconfont.js
├── iconfont.svg
├── iconfont.ttf
└── iconfont.woff

6. 拷贝完成后通过以下命令完成转换：
   $ python3 ./translate_icon_font_from_css_to_dart.py
7. 其中转换时需要 iconfont.ttf 和 iconfont.css 文件;实际项目运行时只需要 iconfont.ttf 文件。其余文件不需要。
8. 开发时，通过右键 demo_fontclass.html 使用 Chrome 打开来查看图标对应的 unicode 码，使用时，将中划线“-”替换为下划线“_”。
*/
}