import 'package:flutter/material.dart';
import 'package:safety_platform/theme.dart';
import 'package:safety_platform/iconfont.dart';
import 'package:safety_platform/authentication.dart';
import 'package:safety_platform/models/login_user.dart';
class MineInfoPage extends StatefulWidget {
  State<StatefulWidget> createState() => _MineInfoPageState();
}

class _MineInfoPageState extends State<MineInfoPage> {
  LoginUser user ;
  void initState() {
     user = getCurrentUser();

    print('user ==> $user');
  }
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: new Text("我的"),
          elevation: 0,
          //     leading: appLeading(context),
          backgroundColor: Color.fromARGB(255, 27, 130, 210),
          centerTitle: true, //设置标题是否局中
        ),
        body: _body(context));
  }

  Widget _warp(Widget child) {
    return DecoratedBox(
      child: child,
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(width: 0.5, color: borderColor))),
    );
  }

  Widget _body(BuildContext context) {
    ListTile mine = ListTile(
      title: Text('${user.screenName}',
          style: TextStyle(
            fontSize: 16,
          )),
      // isThreeLine: true,
      subtitle: Text('${user.role}', style: TextStyle(color: Colors.grey)),
    );
    Widget r = Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(right: 8.00),
          child: Text(
            "v1.0.0",
            style: TextStyle(color: Colors.grey),
          ),
        ),
        Icon(MyIconFont.forward, color: Colors.grey)
      ],
    );
    ListTile version = ListTile(
        title: Text('当前版本'),
        leading: Icon(MyIconFont.update, color: Theme.of(context).primaryColor),
        trailing: r //Icon(MyIconFont.forward, color: Colors.grey),
        // subtitle: Text('1.0.0'),
        // trailing: Icon(MyIconFont.fo),
        );
    ListTile version1 = ListTile(
      title: Text('我的信息'),
      leading: Icon(MyIconFont.pingjia, color: Theme.of(context).primaryColor),
      trailing: Icon(MyIconFont.forward, color: Colors.grey),
    );
    ListTile info = ListTile(
        leading: Icon(
          MyIconFont.yonghuming,
          color: Theme.of(context).primaryColor,
        ),
        trailing: Icon(MyIconFont.forward, color: Colors.grey),
        title: Text('关于我们'));
    return Container(
      color: Colors.white,
      child: ListView(children: [
        _warp(mine),
        _warp(version),
        _warp(version1),
        _warp(info),
      ]),
    );
  }
}
