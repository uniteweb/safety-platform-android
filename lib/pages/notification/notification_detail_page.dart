// import 'package:'
import 'package:flutter/material.dart';
import 'package:safety_platform/models/received_notification.dart';
import 'package:safety_platform/models/notification.dart' as no;
import 'package:safety_platform/pages/notification/services.dart';
import 'package:flutter_native_web/flutter_native_web.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/scheduler.dart';
import 'package:safety_platform/components/button.dart';
import 'package:safety_platform/bloc/notification/notification.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:safety_platform/bloc/notification/notification.dart';

class NotificationDetailPage extends StatefulWidget {
  @override
  _NotificationDetailPageState createState() =>
      new _NotificationDetailPageState();

  final NotificationOperationState state;
  NotificationDetailPage(this.state);
}

class _NotificationDetailPageState extends State<NotificationDetailPage> {
  // NotificationBloc _bloc;
  @override
  void initState() {
    super.initState();
    _bloc = NotificationOperationBloc(initial: widget.state);
    // _bloc.dispatch(ReadEvent());
  }

  @override
  void dispose() {
    super.dispose();
    _bloc.dispose();
  }

  NotificationOperationBloc _bloc;
  _body(NotificationOperationState state) {
    print('通知详情 $state');
    if (state is NotificationSuccess) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Fluttertoast.showToast(msg: '签阅成功');
        Navigator.of(context).pop();
      });
      // NotificationBloc bloc = BlocProvider.of<NotificationBloc>(context);
      // bloc.dispatch(NotficationRefreshEvent(throwLoadSt?ate: false));
    } else {
      // return Center(child: Text('s'),);
      print('显示');
      return ReceivedNotificationWidget(state.data);
    }
    // child:_content(),
    // child: SingleChildScrollView(
    //   child: Html(data: html)));
  }

//  _content(){
//     child: SingleChildScrollView(
//       child: Html(data: html);
//  }
  @override
  Widget build(BuildContext context) {
    Widget bottom = Container();
    return BlocBuilder(
        bloc: _bloc,
        builder: (BuildContext buildContext, NotificationOperationState state) {
          if (state.data != null) {
            if (!state.data.read) {
              Future.delayed(Duration(seconds: 3)).then((_) {
                if (mounted) {
                  _bloc.dispatch(ReadEvent(state.data));
                }
              });
              // _bloc.dispatch(ReadEvent());
            }
          }
          if (!state.data.signed) {
            bottom = Container(
              width: MediaQuery.of(context).size.width,
              height: 48,
              color: Colors.white,
              child: Button(
                loading: state.submitting,
                buttonColor: Colors.white,
                onPressed: () {
                  _bloc.dispatch(SignEvent(state.data));
                },
                text: '签阅',
                textSize: 18,
                textColor: Color.fromARGB(255, 27, 130, 210),
              ),
            );
          } else {
            bottom = Container(
              width: MediaQuery.of(context).size.width,
              height: 48,
              color: Colors.white,
              child: Container(
                child: Center(
                  child: Text(
                    '已签阅',
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                    ),
                  ),
                ),
                // loading: state.submitting,
                // buttonColor: Colors.white,
                // onPressed: () {
                // _bloc.dispatch(SignEvent(state.data));
              ),
            );
          }

          if (state is SubmittingSuccessState) {
            Fluttertoast.showToast(msg: '提交签阅成功');
            SchedulerBinding.instance.addPostFrameCallback((_) {
              Navigator.of(context).pop(state.data);
            });
          }
          return Scaffold(
              appBar: AppBar(
                title: Text('通知详情'),
                elevation: 0,
                backgroundColor: Color.fromARGB(255, 27, 130, 210),
                centerTitle: true, //设置标题是否局中
              ),
              bottomNavigationBar: bottom,
              // bottomNavigationBar: Container(
              //   width: MediaQuery.of(context).size.width,
              //   height: 48,
              //   color: Colors.white,
              //   child: Button(
              //     loading: state.submitting,
              //     buttonColor: Colors.white,
              //     onPressed: () {
              //       _bloc.dispatch(SignEvent(state.data));
              //     },
              //     text: '签阅',
              //     textSize: 18,
              //     textColor: Color.fromARGB(255, 27, 130, 210),
              //   ),
              // ),
              body: _body(state));
        });
  }
}

class ReceivedNotificationWidget extends StatelessWidget {
  final ReceivedNotification notification;

  ReceivedNotificationWidget(this.notification);
  Widget build(BuildContext buildContext) {
    return Material(
      child: Column(
        children: <Widget>[
          _title(notification),
          Expanded(child: _content(notification)),
        ],
      ),
    );
  }

  Widget _content(ReceivedNotification receivedNotification) {
    no.Notification notification = receivedNotification.notification;
    return ListView.builder(
      itemBuilder: (BuildContext buildContext, int index) {
        if (index == 0)
          return Html(
            defaultTextStyle: TextStyle(color: Colors.black87, fontSize: 16),
            data: notification.content,
            padding: EdgeInsets.all(8),
          );
        // else
        //   return _attachments();
      },
      itemCount: 1,
    );
    //  return SingleChildScrollView(

    //    padding: EdgeInsets.all(8), child: Html(data: widget.notification.content),);
  }

  Widget _title(ReceivedNotification receivedNotification) {
    no.Notification notification = receivedNotification.notification;
    // print("_title");
    // Widget w = Container(height: 200,color:Colors.red, child:
    //  Text('222222'),);
    // print("_titl2");
    // return w;

    // return ("TITE2")
    return Column(
      // mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Container(
          // margin: EdgeInsets.all(16),
          // color: Colors.red,
          decoration: BoxDecoration(
              border: Border(
                  bottom: BorderSide(
                      color: Color.fromARGB(255, 241, 241, 241), width: 1))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(8),
                child: Text(
                  '${notification.title}',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Padding(
                  padding: EdgeInsets.all(8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('${notification.createUser.screenName}'),
                      Text(
                          '${notification.datePublished.year}-${notification.datePublished.month}-${notification.datePublished.day}')
                    ],
                  )),
            ],
          ),
        ),
      ],
      // ),
    );
  }
}
