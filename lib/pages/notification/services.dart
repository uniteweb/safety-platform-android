import 'package:safety_platform/http.dart';
import 'package:safety_platform/models/received_notification_page.dart';


Future<ReceivedNotificationPage> loadReceivedNotification(
    {int page: 1, int pageSize: 10, String keyword}) async {
  final String url = '/document/received_notification/';
  Map<String, String> params = Map();

  params['page'] = page.toString();
  params['page_size'] = pageSize.toString();
  params['q'] = keyword;
  print('loadReceivedNotification $params');

  var data = await httpGet('$url', data: params);

  return ReceivedNotificationPage.fromJson(data);
}

Future<bool> updateReceivedNotification(
    int id, Map<String, dynamic> param) async {
  final String url = '/document/received_notification/$id/';

  var resp = await httpPut('$url', data: param);

  return true;
}

Future<bool> markRead(int id) async {
  Map<String,dynamic>  params = Map();
  params['read'] = true;
  return updateReceivedNotification(id, params);
}

Future<bool> markSigned(int id) async {
  Map<String,dynamic>  params = Map();
  params['signed'] = true;
  return updateReceivedNotification(id, params);
}