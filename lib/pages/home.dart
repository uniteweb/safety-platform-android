import 'package:flutter/material.dart';
import 'notification_list.dart';
import 'package:safety_platform/pages/task/history_page.dart';
import 'package:safety_platform/pages/hidden_danger/history.dart';
import 'package:safety_platform/components/carousel_slider.dart';
import '../iconfont.dart';
import 'package:safety_platform/pages/risk_unit/list.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => new _HomeState();
}

class _HomeState extends State<Home> {
  _basicInfo() {
    return Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                // Container(
                //   color: Colors.yellow,
                // ),
                Padding(
                  padding: EdgeInsets.fromLTRB(18.0, 8.0, 18.0, 8.0),
                  child: Text('快捷功能'),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(18.0, 8, 18.0, 8),
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          CircleAvatar(
                            backgroundColor: Color.fromARGB(255, 67, 198, 104),
                            radius: 20,
                            child: Icon(
                              MyIconFont.note,
                              color: Colors.white,
                            ),
                          ),
                          Padding(
                            child: InkWell(
                              child: Text('检查历史'),
                              onTap: () {
                                print('检查历史');
                                Navigator.of(context)
                                    .push(new MaterialPageRoute(builder: (_) {
                                  return null; //TODO
                                }));
                              },
                            ),
                            padding: EdgeInsets.fromLTRB(0, 4, 0, 4),
                          )
                        ],
                      ),
                      InkWell(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircleAvatar(
                              backgroundColor:
                                  Color.fromARGB(255, 255, 195, 12),
                              radius: 20,
                              child: Icon(
                                MyIconFont.jinggao,
                                color: Colors.white,
                              ),
                            ),
                            Padding(
                              child: Text('隐患历史'),
                              padding: EdgeInsets.fromLTRB(0, 4, 0, 4),
                            )
                          ],
                        ),
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (_) => HiddenDangerHistory()));
                        },
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(
                              context, 'received_notification_list');
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircleAvatar(
                              backgroundColor:
                                  Color.fromARGB(255, 27, 130, 210),
                              radius: 20,
                              child: Icon(
                                MyIconFont.lajitong,
                                color: Colors.white,
                              ),
                            ),
                            Padding(
                              child: Text('政策通知'),
                              padding: EdgeInsets.fromLTRB(0, 4, 0, 4),
                            )
                          ],
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          CircleAvatar(
                            backgroundColor: Color.fromARGB(255, 255, 83, 87),
                            radius: 20,
                            child: Icon(
                              MyIconFont.icotianping,
                              color: Colors.white,
                            ),
                          ),
                          Padding(
                            child: Text('法律法规'),
                            padding: EdgeInsets.fromLTRB(0, 4, 0, 4),
                          )
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ],
        ));
  }

  _shortcatWidget(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      // height: 60,
      color: Colors.white,
      padding: EdgeInsets.all(8),
      child: Row(
        children: <Widget>[
          Expanded(
            child: GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, 'todayPlan');
                },
                child: Container(
                  height: 60,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(18.0, 4, 18.0, 4.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            mainAxisSize: MainAxisSize.max,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Text(
                                '定期检查',
                                style: TextStyle(
                                  fontSize: 16,
                                  height: 1.2,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.blue,
                                ),
                              ),
                              Text(
                                '定期安全检查',
                                style: TextStyle(
                                  fontSize: 12,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Image.asset(
                          'assets/inspection.png',
                          width: 36,
                          height: 36,
                        ),
                      )
                    ],
                  ),
                )),
            flex: 1,
          ),
          Expanded(
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (_) => RiskUnitListPage()));
              },
              child: Container(
                height: 60,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(18.0, 4, 18.0, 4.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Text(
                              '隐患自查',
                              style: TextStyle(
                                fontSize: 16,
                                height: 1.2,
                                fontWeight: FontWeight.w400,
                                color: Colors.amber,
                              ),
                            ),
                            Text(
                              '保障生产安全',
                              style: TextStyle(
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Image.asset(
                        'assets/risk_check.png',
                        width: 36,
                        height: 36,
                      ),
                    )
                  ],
                ),
              ),
            ),
            flex: 1,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return 
         Scaffold(
      appBar: AppBar(
        title: new Text("安检通"),
        elevation: 0,
        backgroundColor: Color.fromARGB(255, 27, 130, 210),
        centerTitle: true, //设置标题是否局中
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Color.fromARGB(255, 245, 245, 249),
          child: Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                child: CarouselSliderWidget(
                  height: 120,
                ),
                //   child: new CarouselSlider(
                //       items: [1, 2, 3, 4, 5].map((i) {
                //         return new Builder(
                //           builder: (BuildContext context) {
                //             return new Container(
                //                 width: MediaQuery.of(context).size.width,
                //                 margin:
                //                     new EdgeInsets.symmetric(horizontal: 5.0),
                //                 decoration:
                //                     new BoxDecoration(color: Colors.amber),
                //                 child: new Image.asset(
                //                   'assets/bannerx.png',
                //                   fit: BoxFit.fill,
                //                 ));
                //           },
                //         );
                //       }).toList(),
                //       height: 120.0,
                //       autoPlay: true),
                // ),
              ),
              Column(
                children: <Widget>[
                  _shortcatWidget(context),
                  _basicInfo(),
                  NotificationList(),
                ],
              ),
            ],
          ),
        ),
      
    ));
  }
}
