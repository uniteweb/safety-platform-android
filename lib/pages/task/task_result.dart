import 'package:flutter/material.dart';
import 'package:safety_platform/models/task.dart';
import 'package:safety_platform/models/task_item.dart';
import 'package:safety_platform/models/hidden_danger.dart';

import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:safety_platform/pages/hidden_danger/page.dart';
import 'package:safety_platform/components/task_item.dart';

class TaskResultPage extends StatefulWidget {
  TaskResultPage(this._task);
  final Task _task;
  @override
  State<StatefulWidget> createState() {
    return _TaskResultState();
  }
}

class _TaskResultState extends State<TaskResultPage> {
  List<TaskItem> _negativeItems;
  @override
  void initState() {
    super.initState();
    _negativeItems =
        widget._task.items.where((f) => f.result != f.rightAnswer).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // leading: appLeading(context),
        title: Text('检查结果'),
        elevation: 0,
        backgroundColor: Theme.of(context).primaryColor,
        centerTitle: true, //设置标题是否局中
      ),
      body: _body(),
    );
  }

  Widget _buildAttachmentWidget(TaskItem item) {
    if (item.attachments == null || item.attachments.length <= 0) {
      return Container();
    } else {
      List<Widget> attachmentWidgets =
          item.attachments.map<Widget>((attachment) {
        Widget child;

        if (attachment.localPath != null) {
          child = Image.file(File(attachment.localPath));
        } else {
          if (attachment.url != null) {
            child = CachedNetworkImage(
              imageUrl: attachment.url,
              placeholder: Center(
                child: SpinKitCircle(
                    size: 50, color: Theme.of(context).primaryColor),
              ),
              errorWidget: new Icon(Icons.error),
              fit: BoxFit.fill,
            );
          }
        }
        return Container(
            // color: Colors.red,
            width: 80,
            height: 80,
            margin: EdgeInsets.fromLTRB(0, 0, 8, 0),
            child: child);
      }).toList();

      return Container(
        padding: EdgeInsets.fromLTRB(8, 4, 8, 4),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            // Divider(
            //     // height
            //     ),
            // Padding(
            //     child: Text(
            //       "检查照片",
            //       style: TextStyle(
            //         fontWeight: FontWeight.w600,
            //       ),
            //     ),
            //     padding: EdgeInsets.symmetric(vertical: 8)),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: attachmentWidgets,
              ),
            ),
          ],
        ),
      );
    }
  }

  Widget _buildRemarkWidget(TaskItem item) {
    String remark = '暂无检查备注';

    return Container(
      padding: EdgeInsets.all(8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // Divider(),
          Padding(
            child: Text(
              "检查备注",
              style: TextStyle(
                fontWeight: FontWeight.w600,
              ),
            ),
            padding: EdgeInsets.symmetric(vertical: 4),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 4),
            child: Text(
              remark,
              style: TextStyle(color: Colors.grey),
            ),
          ),
          // TextField(
          //   controller: TextEditingController.fromValue(
          //     TextEditingValue(
          //       // 设置内容
          //       text: item.remark,
          //     ),
          //   ),
          //   maxLines: 4,
          //   style: TextStyle(
          //     fontSize: 14,
          //     color: Colors.black,
          //   ),
          //   maxLength: 600,
          //   decoration: InputDecoration(
          //     border: InputBorder.none,
          //   ),
          // )
        ],
      ),
    );
  }

  Widget _buildButtonRow(TaskItem item) {
    Widget widget;

    if (item.hiddenDangerId == null) {
      widget = FlatButton(
        color: Colors.red,
        child: Text(
          '隐患登记',
          style: TextStyle(color: Colors.white),
        ),
        onPressed: () => _applyHiddenDanger(item),
      );
    } else {
      widget = Padding(
        padding: EdgeInsets.fromLTRB(0, 8, 8, 8),
        child: Text.rich(
          TextSpan(
              text: '隐患编号: ',
              children: <TextSpan>[TextSpan(text: '${item.hiddenDangerId}')]),
        ),
      );
      // widget =  Row(
      //   children: <Widget>[
      //     Padding(
      //       padding: EdgeInsets.only(right: 8),
      //       child:
      //     Text('隐患编号:',)
      //     ,),
      //     Text('${item.hiddenDangerId}'),
      //   ],
      // );
    }
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        // color: Colors.red,
        children: <Widget>[
          Expanded(child: widget),
        ]);
  }

  _applyHiddenDanger(TaskItem item) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return TaskItemHiddenDangerPage(
        this.widget._task.id,
        item.id,
        risk: widget._task.risk,
        content: item.remark,
        traceNo: widget._task.id,
        attachments: item.attachments,
      );
    })).then((hd) {
      if (hd != null) {
        if (mounted) {
          setState(() {
            item.hiddenDangerId = hd.id;
          });
        }
      }
    });
  }

  // Widget _buildNegativeItem(BuildContext context, int index) {
  //   TaskItem item = _negativeItems[index];
  //   return Card(
  //     margin: EdgeInsets.all(8),
  //     child: Column(
  //       crossAxisAlignment: CrossAxisAlignment.start,
  //       children: <Widget>[
  //         Padding(
  //           padding: EdgeInsets.all(8),
  //           child: Column(
  //               crossAxisAlignment: CrossAxisAlignment.start,
  //               children: <Widget>[
  //                 // Padding(
  //                 //   child: Text('检查项目',
  //                 //       style: TextStyle(
  //                 //         fontWeight: FontWeight.w600,
  //                 //       )),
  //                 //   padding: EdgeInsets.symmetric(vertical: 8),
  //                 // ),
  //                 Text(item.content,
  //                     style: TextStyle(
  //                       fontSize: 16,
  //                     ))
  //               ]), //检查项目内容
  //         ),
  //         _buildAttachmentWidget(item),
  //         _buildRemarkWidget(item),
  //         Divider(
  //           height: 0.5,
  //         ),
  //         _buildButtonRow(item),
  //       ],
  //     ),
  //   );
  // }

  Widget _buildNegativeItem(BuildContext context, int index) {
    TaskItem item = _negativeItems[index];

    return Card(
      elevation: 0.2,
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 8),
        child: Column(children: <Widget>[
          TaskItemWidget(item),
          _buildButtonRow(item)
          // Container(
          //   height: 4,
          //   color: Colors.grey,
          // )
        ]),
      ),
    );
  }

  Widget _buildNegativeItemList() {
    return DecoratedBox(
      decoration: BoxDecoration(color: Colors.transparent),
      child: ListView.builder(
        // padding: EdgeInsets.only(left: 8),
        itemBuilder: (context, index) => _buildNegativeItem(context, index),
        itemCount: _negativeItems.length,
      ),
    );
  }

  _body() {
    return _buildNegativeItemList();
  }
}
