/*import 'package:flutter/material.dart';
import 'package:safety_platform/models/task.dart';
import 'package:safety_platform/models/task_item.dart';
import 'package:safety_platform/theme.dart';
import 'package:safety_platform/models/hidden_danger.dart';

import 'package:safety_platform/components/loading.dart';
import 'package:safety_platform/components/hidden_danger.dart';
import 'package:safety_platform/pages/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:cached_network_image/cached_network_image.dart';

class TaskReportPage extends StatefulWidget {
  final Task _task;

  TaskReportPage(this._task);
  State<StatefulWidget> createState() => _TaskReportPageState();
}



class _TaskReportPageState extends State<TaskReportPage> {
  List<HiddenDanger> hiddenDangers;
  @override
  void initState() {
    fetchHiddenDangerFromTask(widget._task).then((hiddenDangers) {
      if (mounted) {
        setState(() {
          this.hiddenDangers = hiddenDangers;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // leading: appLeading(context),
        title: Text('检查报告'),
        elevation: 0,
        backgroundColor: Theme.of(context).primaryColor,
        centerTitle: true, //设置标题是否局中
      ),
      body: SingleChildScrollView(child: _body(context)),
    );
  }

  Widget _buildHiddenDangers(Task task) {
    if (hiddenDangers == null) {
      return Container(
        padding: EdgeInsets.all(8),
        child: Loading(),
      );
    }


    List<Widget> hdWidgets = this
        .hiddenDangers
        .map((hd) => HiddenDangerItemWidget(hd, (HiddenDanger danger) => {}))
        .toList();

    if (hdWidgets.length <= 0) {
      return Container();
    }
    return Container(
      // padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          DecoratedBox(
            decoration: BoxDecoration(
              // color: Theme.of(context).backgroundColor,
            ),
            child: Padding(
              padding: EdgeInsets.fromLTRB(16,8,16,8),
              child: Text('相关隐患' ,style: TextStyle(
                fontWeight: FontWeight.w600
              )),
            ),
          ),
          DecoratedBox(child:Column(
            children: hdWidgets,
          ),
          decoration: BoxDecoration(
            color: Colors.white
          ),
          )
        ],
      ),
    );
  }

  Widget _buildAttachment(TaskItem item) {
    if (item.attachments == null || item.attachments.length <= 0) {
      return Container();
    }
    List<Widget> children = item.attachments.map((attach) {
      print(attach.url);
      
      return  Container(
          margin: EdgeInsets.only(right: 8),
          width: 80,
          height: 80,
          child: CachedNetworkImage(
            imageUrl:attach.url,
             fit: BoxFit.fill,
             placeholder: SpinKitCircle(size: 24, color:Theme.of(context).primaryColor),
             errorWidget: Icon(Icons.no_sim),
            ),
           
            
          );
    }).toList();
    
    return SingleChildScrollView( 
      scrollDirection: Axis.horizontal,
      child: Container(
      margin: EdgeInsets.only(top: 8),
      child: Row(
        children: children,
      ),
    ),);
  }

  List<Widget> _buildTaskItemChildren(Task task) {
    List<Widget> children = task.items.map((TaskItem item) {
      Color color = Colors.grey; //.of(context).primaryColor;
      String result = item.result;
      if (result == null) {
        result = '未检查';
      } else if (result == item.rightAnswer) {
        color = rightAnswerColor;
      } else {
        color = negtiveAnswerColor;
      }
      return DecoratedBox(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Padding(
                      child: Container(
                        decoration: BoxDecoration(
                             color: color,
                          borderRadius: BorderRadius.circular(1),
                        ),
                        child: Text(result,
                            style: TextStyle(
                              color: Colors.white,
                            )),
                     
                        padding: EdgeInsets.all(4),
                      ),
                      padding: EdgeInsets.only(right: 8)),
                  Text(
                    item.content,
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      child: Text('备注:', style: secondTextStyle),
                      padding: EdgeInsets.only(bottom: 4),
                    ),
                    Text(item.remark ?? '暂无备注', style: secondTextStyle,
                        overflow: TextOverflow.clip,
                        softWrap:true,
                    ),
                  ],
                ),
              ),
              _buildAttachment(item),
            ],
          ),
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            bottom: BorderSide(
              color: borderColor,
              width: 0.5,
            ),
          ),
        ),
      );
    }).toList();
    return children;
  }

  Widget _body(BuildContext context) {
    List<Widget> itemChild = _buildTaskItemChildren(widget._task);
    return Container(
      // color: Colors.white,
      // padding: EdgeInsets.only(left: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Column(
              children: itemChild,
            ),
            padding: EdgeInsets.only(left: 16),
            color: Colors.white,
          ),
          _buildHiddenDangers(widget._task),
        ],
      ),
    );
  }
}
*/