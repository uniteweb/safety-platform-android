import 'package:flutter/material.dart';

class NotificationList extends StatelessWidget {
  
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(18.0, 8, 18.0, 8),
            child: Text('信息中心'),
          ),
          ListView(
            shrinkWrap: true,
            children: <Widget>[
              buildItem(
                '关于督促生产经营单位建立健全安全生产档案的通知1',
                '政策通知',
                new DateTime(2018),
              ),
              buildItem(
                '关于督促生产经营单位建立健全安全生产档案的通知2',
                '政策通知',
                new DateTime(2018),
              ),
              buildItem(
                '关于督促生产经营单位建立健全安全生产档案的通知3',
                '政策通知',
                new DateTime(2018),
              ),
              buildItem(
                '关于督促生产经营单位建立健全安全生产档案的通知4',
                '政策通知',
                new DateTime(2018),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget buildItem(String title, String type, DateTime publishDate) {
    return SizedBox(
      height: 86,
      child: Container(
        margin: EdgeInsets.fromLTRB(8, 0, 8, 8),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5.0),
          border: new Border.all(width: 1.0, color: Colors.white),
        ),
        padding: EdgeInsets.fromLTRB(10.0, 8, 10.0, 8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(title),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Material(
                    borderRadius: BorderRadius.circular(20.0),
                    shadowColor: Colors.blue,
                    // elevation: 5.0,
                    color: Colors.blue,
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(12.0, 4.0, 12.0, 4.0),
                      child: Text(type,
                          style: TextStyle(color: Colors.white, fontSize: 12)),
                    )),
                Text(
                    '发布日期: ${publishDate.year}.${publishDate.month}.${publishDate.day}'),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class _Notification {
  String title = '';
  String type = '';
  DateTime publishDate = null;

  _Notification(this.title, this.type, this.publishDate);
}
