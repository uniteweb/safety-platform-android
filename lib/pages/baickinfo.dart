import 'package:flutter/material.dart';
import '../iconfont.dart';

class BasicInfo extends StatelessWidget {
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                // Container(
                //   color: Colors.yellow,
                // ),
                Padding(
                  padding: EdgeInsets.fromLTRB(18.0, 8.0, 18.0, 8.0),
                  child: Text('快捷功能'),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(18.0, 8, 18.0, 8),
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          CircleAvatar(
                            backgroundColor: Color.fromARGB(255, 67, 198, 104),
                            radius: 20,
                            child: Icon(
                              MyIconFont.note,
                              color: Colors.white,
                            ),
                          ),
                          Padding(
                            child: Text('检查历史'),
                            padding: EdgeInsets.fromLTRB(0, 4, 0, 4),
                          )
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          CircleAvatar(
                            backgroundColor: Color.fromARGB(255, 255, 195, 12),
                            radius: 20,
                            child: Icon(
                              MyIconFont.jinggao,
                              color: Colors.white,
                            ),
                          ),
                          Padding(
                            child: Text('隐患历史'),
                            padding: EdgeInsets.fromLTRB(0, 4, 0, 4),
                          )
                        ],
                      ),
                      GestureDetector(
                        onTap: (){
                          Navigator.pushNamed(context, 'received_notification_list');
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircleAvatar(
                              backgroundColor:
                                  Color.fromARGB(255, 27, 130, 210),
                              radius: 20,
                              child: Icon(
                                MyIconFont.lajitong,
                                color: Colors.white,
                              ),
                            ),
                            Padding(
                              child: Text('政策通知'),
                              padding: EdgeInsets.fromLTRB(0, 4, 0, 4),
                            )
                          ],
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          CircleAvatar(
                            backgroundColor: Color.fromARGB(255, 255, 83, 87),
                            radius: 20,
                            child: Icon(
                              MyIconFont.icotianping,
                              color: Colors.white,
                            ),
                          ),
                          Padding(
                            child: Text('法律法规'),
                            padding: EdgeInsets.fromLTRB(0, 4, 0, 4),
                          )
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ],
        ));
  }
}
