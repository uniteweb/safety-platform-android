import 'package:safety_platform/authentication.dart' as http;

Future<Null> login(String userName, String password) async {
  try {
    await http.login(userName, password);
  } catch (e) {
    throw e;
  }
}
