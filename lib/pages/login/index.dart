import 'package:flutter/material.dart';
import 'package:safety_platform/iconfont.dart';
import 'loging_service.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LoginState();
  }
}

class _LoginState extends State<LoginPage> {
  String phoneError = '';
  String passwordError;
  TextEditingController _loginNameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  @override
  void dispose() {
    super.dispose();
    _loginNameController.dispose();
    _passwordController.dispose();
  }

  @override
  void initState() {
    super.initState();
    print('Login State => initState');
    // _loginNameController.text = "sss";
    _loginNameController.addListener(() {
      if (_loginNameController.text == null ||
          _loginNameController.text.length <= 0) {
        setState(() {
          phoneError = '请输入登录手机号';
        });
      } else {
        setState(() {
          phoneError = null;
        });
      }
    });
    _passwordController.addListener(() {
      if (_passwordController.text == null ||
          _passwordController.text.length <= 0) {
        setState(() {
          passwordError = '请输入密码';
        });
      } else {
        setState(() {
          passwordError = null;
        });
      }
    });
  }

  _login() {
    print('login');
    var phone = _loginNameController.text;

    var password = _passwordController.text;

    if (phone == null || phone.length <= 0) {
      setState(() {
        phoneError = '请输入登录手机号';
      });
      return;
    }

    if (password == null || password.length <= 0) {
      setState(() {
        passwordError = '请输入密码';
      });
      return;
    }
    login(phone, password).then((v) {
      Navigator.popAndPushNamed(context, 'main');
    }).catchError((onError) {
      print("LoginError $onError");
      showDialog(
          context: context,
          builder: (BuildContext context) => AlertDialog(
                title: Text('登录错误'),
                content: Text(onError.message),
              ));
      _loginNameController.clear();
      _passwordController.clear();
      return;
    });

    // showDialog(
    //       context: context,
    //       builder: (context) => AlertDialog(
    //             title: Text('错误'),
    //             content: Text('手机号格式不正确'),
    //           ));

    // }
  }

  _loginWidget(BuildContext context) {
    var phoneDecration = InputDecoration(
      hintText: '请输入手机号',
      // errorText: phoneError == null? '':'$phoneError',
      // prefixIcon: Container(
      //     // color: Colors.red,
      //     padding: EdgeInsets.fromLTRB(0, 0, 0, 8),
      //     child: Icon(
      //       MyIconFont.phone,
      //       size: 18,
      //       color: Colors.grey,
      //     )),

      icon: Icon(MyIconFont.phone, size: 18),
      border: UnderlineInputBorder(borderSide: BorderSide(color: Colors.red)),
    );
    var passwordDecration = InputDecoration(
      hintText: '请输入密码',
      // errorText: phoneError == null? '':'$phoneError',
      // prefixIcon: Container(
      //     // color: Colors.red,
      //     padding: EdgeInsets.fromLTRB(0, 0, 0, 8),
      //     child: Icon(
      //       MyIconFont.phone,
      //       size: 18,
      //       color: Colors.grey,
      //     )),

      icon: Icon(MyIconFont.phone, size: 18),
      border: UnderlineInputBorder(borderSide: BorderSide(color: Colors.red)),
    );
    // InputDecoration passwordDecoration ;
    if (phoneError != null) {
      phoneDecration = phoneDecration.copyWith(errorText: phoneError);
      // phoneDecration.counterStyle = phoneError;
    }
    if (passwordError != null) {
      passwordDecration = passwordDecration.copyWith(errorText: passwordError);
    }
    return Card(
      margin: EdgeInsets.all(16),
      child: Container(
        height: 240,
        padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            // Container(
            //   color: Colors.red,
            //   child: Row(
            //     children: <Widget>[
            //       Icon(MyIconFont.phone),
            //       TextField(),
            //     ],
            //   ),
            // ),
            TextField(
              keyboardAppearance: Brightness.dark,
              controller: _loginNameController,
              keyboardType: TextInputType.number,
              decoration: phoneDecration,
            ),
            TextField(
              obscureText: true,
              controller: _passwordController,
              decoration: passwordDecration,
            ),
            //  TextField(
            //     decoration: InputDecoration(
            //   hintText: '请输入密码',
            //   prefixIcon: Container(
            //       margin: EdgeInsets.only(right: 16),
            //       child: Icon(MyIconFont.phone, size: 18,color: Colors.grey,)),
            //   border: UnderlineInputBorder(
            //       borderSide: BorderSide(color: Colors.red)),
            // )),
            Container(
              alignment: Alignment.centerRight,
              child: Text('找回密码',
                  style: TextStyle(color: Color.fromARGB(255, 27, 130, 210))),
              // padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(8, 0, 0, 8),
                    child: RaisedButton(
                      onPressed: () => _login(),
                      child: Text(
                        '登陆',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ],
            )
            // )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      theme: ThemeData(
          primaryColor: Color.fromARGB(255, 27, 130, 210),
          buttonTheme: ButtonThemeData(
            buttonColor: Color.fromARGB(255, 27, 130, 210),
            height: 40,
          )),
      home: Scaffold(
        backgroundColor: Color.fromARGB(255, 27, 130, 210),
        resizeToAvoidBottomPadding: false,
        body: Container(
          child: Align(
            alignment: Alignment(0.5, -0.4),
            child: _loginWidget(context),
          ),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: ExactAssetImage("assets/login.jpg"),
              alignment: Alignment.topLeft,
            ),
          ),
        ),
      ),
    );
  }
}
