/*import 'package:flutter/material.dart';
import 'services.dart';

import 'package:safety_platform/models/task_risk_unit.dart';
import 'package:safety_platform/models/task.dart';
import 'package:safety_platform/models/risk.dart';
import 'package:safety_platform/utils.dart';
import 'package:flutter_refresh/flutter_refresh.dart';
import 'package:safety_platform/pages/risk_unit/page.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:safety_platform/models/task_page.dart';

class RiskList extends StatefulWidget {
  @override
  _RiskListState createState() => new _RiskListState();
}

TextStyle whiteText = TextStyle(color: Colors.white);

class _RiskListState extends State<RiskList> {
  @override
  void initState() {
    super.initState();
  }

  TaskPage _page;
  List<TaskRiskUnit> _planList;

  final Color pendigColor = Color.fromARGB(255, 255, 180, 0);
  final Color riskyColor =  Color.fromARGB(255, 139, 0, 0);
  final Color doneColor =  Color.fromARGB(255, 30, 144, 255);
  Widget _statusText(String title, String value, double width) {
    return Container(
      width: width,
      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 8),
            child: Text(
              title,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
          Text(
            value,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: whiteText,
          ),
        ],
      ),
    );
  }

  Text _buildStatusText(String status) {
    TextStyle style;
    switch (status) {
      case 'pending':
        {
          return Text('未检查', style: TextStyle(color: pendigColor));
        }
      case 'done':
        {
          return Text('已检查',
              style: TextStyle(color: doneColor));
        }
      case 'risky':
        {
          return Text('有隐患', style: TextStyle(color: riskyColor));
        }
      default:
        return Text(status);
    }
  }

  List<Widget> _buildRiskItem(List<Task> tasks, BuildContext context) {
    // double statusWidth = 180;

    double statusWidth =
        (MediaQuery.of(context).size.width - 16 * 2 - 8 * 2 - 10) / 3;

    ///MediaQuery.of(context).size.width - 16 - 8 / 3;
    print('statusWidth: ${statusWidth}');
    return tasks.map((task) {
      Risk f = task.risk;
      return GestureDetector(
          onTap: () {
            print("跳转到任务: $f");
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (BuildContext context) {
              return RiskUnitPage(task: task);
            }));
          },
          child: Container(
            margin: EdgeInsets.fromLTRB(8, 8, 8, 8),
            // padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5.0),
                  topRight: Radius.circular(5.0)),
              border: new Border.all(width: 1.0, color: Colors.white),
              color: Colors.white,
            ),
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                            width: MediaQuery.of(context).size.width -
                                16 * 2 -
                                1 * 2,
                            padding: EdgeInsets.fromLTRB(8, 8, 4, 4),
                            // margin: EdgeInsets.fromLTRB(0, 0, 0, 4),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(
                                  '风险点名称',
                                  style: TextStyle(fontWeight: FontWeight.w700),
                                ),
                                _buildStatusText(task.status),
                              ],
                            )),
                        Container(
                          padding: EdgeInsets.fromLTRB(8, 8, 8, 8),
                          width: MediaQuery.of(context).size.width -
                              16 * 2 -
                              1 * 2,
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 4),
                          child: Text('${f.name}',
                              overflow: TextOverflow.ellipsis,
                              maxLines: 6,
                              style: TextStyle(color: Colors.grey)),
                        )
                      ],
                    )
                  ],
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(5.0),
                        bottomLeft: Radius.circular(5.0)),
                    border: new Border.all(width: 1.0, color: Colors.blue),
                    color: Colors.blue,
                  ),
                  margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
                  padding: EdgeInsets.fromLTRB(0, 8, 0, 8),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          _statusText('风险点编号', '${f.code}', statusWidth),
                          _statusText(
                              '风险等级', riskLevelTransform(f.level), statusWidth),
                          _statusText(
                              '负责人', '${f.responsePerson}', statusWidth),
                          // Column(
                          //   children: <Widget>[
                          //     Padding(
                          //       padding: EdgeInsets.fromLTRB(0, 0, 0, 8),
                          //       child: Text(
                          //         '风险等级',
                          //         style: whiteText,
                          //       ),
                          //     ),
                          //     Text(
                          //       '${f.level}',
                          //       style: whiteText,
                          //     ),
                          //   ],
                          // ),
                          // Column(
                          //   children: <Widget>[
                          //     Padding(
                          //       padding: EdgeInsets.fromLTRB(0, 0, 0, 8),
                          //       child: Text(
                          //         '风险类型',
                          //         style: whiteText,
                          //       ),
                          //     ),
                          //     Text(
                          //       '${f.classification.join(",")}',
                          //       style: whiteText,
                          //     ),
                          //   ],
                          // ),
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ));
    }).toList();
  }

  Widget _buildItem(context, index) {
    TaskRiskUnit content = this._planList[index];

    var children = <Widget>[
      Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(8),
        color: Color.fromARGB(255, 27, 130, 210),
        child: Text('单元:${content.unitName}',
            style: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.w600,
            )),
      ),
      Container(
        padding: EdgeInsets.fromLTRB(0, 4, 0, 4),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
              bottom: BorderSide(
                width: 1.0,
                color: Color.fromARGB(255, 241, 241, 241),
                style: BorderStyle.solid,
              ),
            )),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                padding: EdgeInsets.all(8.0),
                width: MediaQuery.of(context).size.width,
                child: Text.rich(
                  TextSpan(text: '编号:', children: <TextSpan>[
                    TextSpan(
                        text: ' ${content.unitCode}',
                        style: TextStyle(
                          color: Colors.grey,
                        ))
                  ]),
                ),
                decoration: BoxDecoration(
                  border: Border(
                      right: BorderSide(
                    width: 1.0,
                    color: Color.fromARGB(255, 241, 241, 241),
                    style: BorderStyle.solid,
                  )),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                padding: EdgeInsets.all(8.0),
                width: MediaQuery.of(context).size.width,
                child: Text.rich(
                  TextSpan(text: '类别:', children: <TextSpan>[
                    TextSpan(
                        text: ' ${content.category.title}',
                        style: TextStyle(
                          color: Colors.grey,
                        ))
                  ]),
                ),
              ),
            ),
          ],
        ),
      ),
      // Container(
      //     margin: EdgeInsets.all(8.0),
      //     width: MediaQuery.of(context).size.width,
      //     color: Colors.white,
      //     child: ListView(
      //       shrinkWrap: true,
      //       children: _buildRiskItem(content, context),
      //     ))
    ];
    if (content.tasks != null) {
      List<Widget> riskWidgets = _buildRiskItem(content.tasks, context);
      children.addAll(riskWidgets);
    }
    return Container(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 16.0),
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: children,
        // Container(
        //   color:Color.fromARGB(255, 27, 130, 210) ,
        //   )
      ),
    );
  }

  Future<Null> _onFooterRefresh() {
    return Future.delayed(Duration(seconds: 1), () {
      if (_page.next == null) {
        print("没有更多数据了");
      } else {
        // loadTodayPlan(page:_page.next,pageSize: _page.pageSize).then(
        //   (res){
        //     this._page = res;
        //     this._planList.addAll(res.results);

        //   }
        // );
      }
    });
  }

  Future<Null> _onHeaderRefresh() {
    return Future.delayed(Duration(seconds: 1), () {
      loadTodayTask().then((res) {
        this._page = res;
        this._planList.clear();
        this._planList.addAll(res.results);
      });
    });
  }

  Widget build(BuildContext context) {
    // _loadData();
    return new Scaffold(
      bottomNavigationBar: null,
      appBar: AppBar(
        // leading: Align(
        //   // onPressed: this.clickedLeadingBtn,
        //   alignment: Alignment.centerLeft,
        //   child: new Icon(
        //     MyIconFont.jiantou_zuo,
        //     size: 32,
        //   ),

        //   // padding: const EdgeInsets.all(5.0),
        // ),

        title: new Text("风险列表"),
        elevation: 0,
        backgroundColor: Color.fromARGB(255, 27, 130, 210),
        centerTitle: true, //设置标题是否局中
      ),
      body: FutureBuilder<TaskPage>(
        future: loadTodayTask(),
        builder: (BuildContext context, AsyncSnapshot<TaskPage> snapshort) {
          switch (snapshort.connectionState) {
            case ConnectionState.waiting:
            case ConnectionState.none:
            case ConnectionState.active:
              {
                print("ConnectionState: ${snapshort.connectionState}");
                return Center(
                    child: SpinKitWave(
                  size: 16,
                  color: Color.fromARGB(255, 27, 130, 210),
                ));
              }
            case ConnectionState.done:
              {
                print("done");
                print(snapshort.error);
                print(snapshort.data);
                if (snapshort.data != null) {
                  _page = snapshort.data;
                  _planList = _page.results;

                  return Refresh(
                    onFooterRefresh: _onFooterRefresh,
                    onHeaderRefresh: _onHeaderRefresh,
                    child: ListView.builder(
                      itemCount: _planList.length,
                      itemBuilder: _buildItem,
                    ),
                  );
                }
                break;
              }
          }
        },
      ),
      // body: ListView.builder(
      //   itemCount: data.length,
      //   itemBuilder: _buildItem,
      // ),
    );
  }
}
*/