import 'package:flutter/material.dart';
import 'package:safety_platform/components/risk_unit.dart';
import 'package:safety_platform/components/loading.dart';
import 'package:safety_platform/models/risk_unit.dart';
import 'package:safety_platform/bloc/risk/risk.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:safety_platform/models/risk.dart';
import 'package:safety_platform/pages/risk_unit/risk_detail.dart';


class RiskUnitDetailPage extends StatefulWidget {
  final RiskUnit riskUnit;

  final String  riskUnitId;
  RiskUnitDetailPage({this.riskUnit, this.riskUnitId});

  @override
  State<StatefulWidget> createState() {
    return _RiskUnitDetailState();
  }
  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //       appBar: AppBar(
  //         title: new Text('风险单元'),
  //         elevation: 0,
  //         backgroundColor: Color.fromARGB(255, 27, 130, 210),
  //         centerTitle: true, //设置标题是否局中
  //       ),
  //       // bottomNavigationBar: _buildBottomNavigationBar(task),
  //       body: _body(context) //_buildBody(context),
  //       );
  // }

  // Widget _body(BuildContext context) {
  //   return RiskUnitWidget(
  //     riskUnit,
  //     onPress: (Risk risk) {
  //       Navigator.of(context).push(MaterialPageRoute(builder: (_) => RiskDetailPage(risk)));
  //     },
  //   );
  // }
}

class _RiskUnitDetailState extends State<RiskUnitDetailPage> {
  
  RiskUnitBloc _bloc;


  @override
  void initState() {
    _bloc = RiskUnitBloc();
    _bloc.dispatch(RiskUnitStartupEvent(
      riskUnitId:widget.riskUnitId,
      riskUnit:widget.riskUnit
    ));
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _bloc.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: _bloc,
      child: BlocBuilder(
        bloc:_bloc,
        builder: (BuildContext context, RiskUnitDetailState state){
          if (state is LoadingRiskUnitDetailState 
              || state is LoadingRiskUnitRiskState) {
            return Loading();
          } 
          if (state is EmptyRiskUnitDetailState) {
            return Container();
          }
              return Scaffold(
        appBar: AppBar(
          title: new Text(state.riskUnit.unitName),
          elevation: 0,
          backgroundColor: Color.fromARGB(255, 27, 130, 210),
          centerTitle: true, //设置标题是否局中
        ),
        // bottomNavigationBar: _buildBottomNavigationBar(task),
        body: RiskUnitWidget(state.riskUnit, 
        onPress: (Risk risk) {
           Navigator.of(context).push(MaterialPageRoute(builder: (_) => RiskDetailPage(risk))); 
        }
        ) //_buildBody(context),
        );
        }
      ),
    );
  }
}