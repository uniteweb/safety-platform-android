import 'package:flutter/material.dart';
import 'package:safety_platform/components/risk.dart';
import 'package:safety_platform/models/risk.dart';

class RiskDetailPage extends StatelessWidget {
  final Risk risk;

  RiskDetailPage(this.risk);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: new Text(risk.name),
          elevation: 0,
          backgroundColor: Color.fromARGB(255, 27, 130, 210),
          centerTitle: true, //设置标题是否局中
        ),
        // bottomNavigationBar: _buildBottomNavigationBar(task),
        body: _body(context) //_buildBody(context),
        );
  }

  Widget _body(BuildContext context) {
    return RiskDetailWidget(risk);
  }
}
