/*(import 'package:flutter/material.dart';
import 'package:safety_platform/pages/task/services.dart';
import 'package:safety_platform/models/risk.dart';
import 'package:safety_platform/models/task.dart';
import 'package:safety_platform/models/solution.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:safety_platform/pages/task/task_detail_page.dart';
import 'package:safety_platform/components/button.dart';
import 'package:safety_platform/pages/task/task_report.dart';
class RiskUnitPage extends StatefulWidget {
  @override
  _RiskUnitState createState() => new _RiskUnitState();

  RiskUnitPage({Key key, this.task});
  Task task;
  // Risk risk;
}

// final solutions = [
//   '使用前对设备进行各项检查，及时排查故障',
//   '作业员必须穿戴合格的劳动防护用品',
//   '操作人员安全操作知识培训常态化',
//   '做好常规保养并做好记录',
// ];

class _RiskUnitState extends State<RiskUnitPage> {
  @override
  void didUpdateWidget(RiskUnitPage oldWidget) {
    print('组件状态改变：didUpdateWidget');
    super.didUpdateWidget(oldWidget);
  }

  void initState() {
    super.initState();
    getTaskDetail(widget.task.id).then((task) {
      setState(() {
        this.task = task;
      });
    });
  }

  Task task;
  @override
  Widget build(BuildContext context) {
    if (task == null) {
      return Center(
          child: SpinKitWave(
        size: 16,
        color: Color.fromARGB(255, 27, 130, 210),
      ));
    }
    return Scaffold(
        appBar: AppBar(
          title: new Text('${task.risk.name}'),
          elevation: 0,
          backgroundColor: Color.fromARGB(255, 27, 130, 210),
          centerTitle: true, //设置标题是否局中
        ),
        bottomNavigationBar: _buildBottomNavigationBar(task),
        body: _body(context) //_buildBody(context),
        );
  }

  _buildBottomNavigationBar(Task task) {
    if (task.status == 'pending') {
      return Container(
        width: MediaQuery.of(context).size.width,
        height: 48,
        color: Colors.white,
        child: Button(
          buttonColor: Colors.white,
          onPressed: () {
            Navigator.of(context)
                .pushReplacement(new MaterialPageRoute(builder: (_) {
              return TaskDetailPage(task);
            }));
          },
          text: '开始检查',
          textSize: 18,
          textColor: Color.fromARGB(255, 27, 130, 210),
        ),
      );
    } else {
     return Container(
        width: MediaQuery.of(context).size.width,
        height: 48,
        color: Colors.white,
        child: Button(
          buttonColor: Colors.white,
          onPressed: () {
            Navigator.of(context)
                .pushReplacement(new MaterialPageRoute(builder: (_) {
              return TaskReportPage(task);
            }));
          },
          text: '查看检查报告',
          textSize: 18,
          textColor: Color.fromARGB(255, 27, 130, 210),
        ),
      );
    }
  }

  _buildDangerWidgets(String danger) {
    List<Widget> dangers =
        danger.split('\r\n').map<Widget>((d) => _desc(d)).toList();
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: dangers,
    );
  }

  _body(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      children: <Widget>[
        Container(
            padding: EdgeInsets.all(8),
            width: MediaQuery.of(context).size.width,
            // height: 180,
            color: Color.fromARGB(255, 27, 130, 210),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _title('危险因素'),
                _buildDangerWidgets(task.risk.dangers),
                _titleWithDesc('管控措施依据的标准和规范', task.risk.standards),
                _titleWithDesc('涉及的隐患分类', task.risk.classification.join(',')),
                _titleWithDesc('责任单位', task.risk.responseUnit),
                Padding(
                  child: _title('应采取的管控措施'),
                  padding: EdgeInsets.only(top: 8),
                )
                // _solution(2,'3'),
              ],
            )),
        Container(
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            child: _solutionWidget(context)),
        Container(
          color: Colors.white,
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(8, 16, 8, 8),
          child: _remarkArea(),
        ),
        Container(
          height: 48,
          child: _responseArea(),
          width: MediaQuery.of(context).size.width,
          color: Colors.blue,
        )
      ],
    ));
  }

  Widget _solutionItem(Solution solution, double width) {
    return Container(
      padding: EdgeInsets.fromLTRB(0, 12, 0, 12),
      // color: Colors.red,
      child: Text(solution.title),
      width: width,
      decoration: BoxDecoration(
          // color: Colors.red,
          border: Border(
              bottom: BorderSide(
        color: Color.fromARGB(255, 241, 241, 241),
        width: 1,
      ))),
    );
  }

  _responseArea() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(
          child: Container(
            // color: Colors.red,
            decoration: BoxDecoration(
                border:
                    Border(right: BorderSide(width: 0.5, color: Colors.white))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  '负责人',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.w600),
                ),
                Text(
                  task.risk.responsePerson,
                  style: TextStyle(color: Colors.white),
                ),
              ],
            ),
          ),
        ),
        Expanded(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  '联系电话',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.w600),
                ),
                Text(
                  task.risk.contactPhone,
                  style: TextStyle(color: Colors.white),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  _remarkArea() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 4),
            child: Text(
              '备注: ',
              maxLines: 4,
              style: TextStyle(
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 4),
            child: Text(
              '暂无风险点其他说明',
              maxLines: 4,
              style: TextStyle(),
            ),
          )
        ],
      ),
    );
  }

  _solutionWidget(BuildContext buildContext) {
    print("风险管控措施${task.risk.solutions}");
    double width = MediaQuery.of(buildContext).size.width - 8 * 2;
    Risk risk = task.risk;
    if (risk.solutions != null && risk.solutions.length > 0) {
      List<Widget> widges = risk.solutions.map<Widget>((s) {
        return Container(
          padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
          margin: EdgeInsets.fromLTRB(0, 4, 0, 0),
          child: _solutionItem(s, width),
        );
      }).toList();
      return Column(
        children: widges,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
      );
    }
    return null;
  }

  _titleWithDesc(String title, String desc) {
    return Container(
        margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _title(title),
            _desc(desc),
          ],
        ));
  }

  _title(String title) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 4),
      child: Text(
        '$title: ',
        maxLines: 4,
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w700,
        ),
      ),
    );
  }

  _desc(String desc) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 4),
      child: Text(
        desc,
        maxLines: 4,
        style: TextStyle(
          color: Colors.white,
        ),
      ),
    );
  }
  // _titleWithDesc(String title, String desc) {
  //   return Container(
  //       child: Column(
  //     mainAxisAlignment: MainAxisAlignment.start,
  //     crossAxisAlignment: CrossAxisAlignment.start,
  //     children: <Widget>[
  //       _title(title),
  //       Padding(
  //         padding: EdgeInsets.fromLTRB(0, 0, 0, 4),
  //         child: Text(
  //           desc,
  //           maxLines: 1,
  //           style: TextStyle(
  //             color: Colors.white,
  //           ),
  //         ),
  //       )
  //     ],
  //   ));
  // }
}
*/