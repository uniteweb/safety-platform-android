import 'package:flutter/material.dart';
import 'package:safety_platform/bloc/message/message.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:safety_platform/models/message.dart';
import 'package:safety_platform/utils.dart';
class MessageListPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MessageListPageState();
  }
}

class _MessageListPageState extends State<MessageListPage> {
   MessageBloc _bloc;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: _bloc,
        builder: (BuildContext context, MessageState state) {
          return Scaffold(
              appBar: AppBar(
                title: new Text("消息中心"),
                elevation: 0,
                //     leading: appLeading(context),
                // backgroundColor: Color.fromARGB(255, 27, 130, 210),
                centerTitle: true, //设置标题是否局中
              ),
              body: _body(context, state));
        });
  }

  @override
  void initState() {
    super.initState();
    _bloc  = MessageBloc();
    _bloc.dispatch(MessageRefreshEvent());
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }

  Widget _body(BuildContext context, MessageState state) {
    if (state == null) {
      return Center(
        child: Text('nodate'),
      );
    } else if (state is MessageUpdatedState) {
      print('child => ${state.messages}');
      return MessageListWidget(state.messages);
    }
    return Container(child: Text("sss"));
  }
}

class MessageListWidget extends StatelessWidget {
  final List<Message> messages;

  const MessageListWidget(this.messages);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) => _buildItem(context, index),
      itemCount: messages.length,
    );
  }

  Widget _buildItem(BuildContext context, int index) {
    Message message = messages[index];
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: double.infinity,
            margin: EdgeInsets.all(8),
            child: Center(
              child: Text(
                formatDate(message.created),
                style:TextStyle(color: Colors.black54),
               )),
          ),
          Card(
            margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
            color: Colors.white,
            elevation: 0,
            child: Container(
                padding: EdgeInsets.all(8),
                width: double.infinity,
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      child: Text(
                        '业务提醒',
                        style: TextStyle(fontSize: 16, color: Colors.black54),
                      ),
                      padding: EdgeInsets.symmetric(vertical: 4),
                    ),
                    Text(
                      '${message.content}',
                      style: TextStyle(color: Colors.black54),
                    )
                  ],
                )),
          )
        ],
      ),
    );
  }
}
