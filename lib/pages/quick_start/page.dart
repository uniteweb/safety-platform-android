import 'package:flutter/material.dart';
import 'package:safety_platform/iconfont.dart';
import 'package:safety_platform/utils.dart';
import 'package:safety_platform/pages/hidden_danger/page.dart';
import 'package:flutter_plugin_wechat_scan/flutter_plugin_wechat_scan.dart';
// import 'package:barcode_scan/barcode_scan.dart';

class QuickStart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: new Text("快速检查"),
          elevation: 0,
          //     leading: appLeading(context),
          backgroundColor: Color.fromARGB(255, 27, 130, 210),
          centerTitle: true, //设置标题是否局中
        ),
        body: _body(context));
  }

  _body(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        padding: EdgeInsets.fromLTRB(16, 32, 16, 32),
        child: Column(children: <Widget>[
          Expanded(
            flex: 1,
            child: InkWell(
              onTap: (){
                FlutterPluginWechatScan.scan(title: '风险点扫描').then(
                  (qrText) {
                    print('二维码扫描结果: $qrText');
                  }
                ).catchError((e) => print('扫描二维码失败 $e'));
              },
              child: Container(
                // color: Colors.red,

                child: Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                      Center(
                          child: CircleAvatar(
                        radius: 50,
                        child: Icon(
                          MyIconFont.scan,
                          color: Colors.white,
                          size: 60,
                        ),
                      )),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        // color: Colors.red,
                        margin: EdgeInsets.only(top: 16),
                        child: Center(
                            child: Text(
                          '隐患扫描',
                          style: TextStyle(
                              color: Colors.grey,
                              fontSize: 18,
                              fontWeight: FontWeight.w500),
                        )),
                      )
                    ])),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: InkWell(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return CreateHiddenDangerPgae();
                }));
              },
              child: Container(
                // color: Colors.red,

                child: Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                      Center(
                          child: CircleAvatar(
                        radius: 50,
                        child: Icon(
                          MyIconFont.xiangji,
                          color: Colors.white,
                          size: 60,
                        ),
                      )),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        // color: Colors.red,
                        margin: EdgeInsets.only(top: 16),
                        child: Center(
                            child: Text(
                          '添加隐患',
                          style: TextStyle(
                              color: Colors.grey,
                              fontSize: 18,
                              fontWeight: FontWeight.w500),
                        )),
                      )
                    ])),
              ),
            ),
          ),
        ]));
  }
}
