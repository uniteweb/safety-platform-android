import 'package:flutter/material.dart';

class HiddenDangerTracePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HiddenDanggerTracePageState();
}

class _HiddenDanggerTracePageState extends State<HiddenDangerTracePage> {
  List<String> hiddenDangers = List();

  int _currentStep = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      // floatingActionButton: new FloatingActionButton(
      //   foregroundColor: Colors.white,
      //   elevation: 10.0,
      //   onPressed: () {},
      //   tooltip: '查看隐患',
      //   child: new Icon(Icons.add),
      // ),
      appBar: AppBar(
        title: new Text("添加整改信息"),
        elevation: 0,

        //     leading: appLeading(context),
        backgroundColor: Color.fromARGB(255, 27, 130, 210),
        centerTitle: true, //设置标题是否局中
      ),
      body: SingleChildScrollView(
        child: _body(context),
      ),
      // bottomNavigationBar: _bottomVavigationBar(),
    );
  }

  void _onStepTapped(int index) {
    setState(() {
      _currentStep = index;
    });
  }

  Widget _buildItem(BuildContext context, int index) {
    List<Step> steps = <Step>[
      Step(
        title: Text('已登记'),
        content: Container(
          // color: Colors.red,
          padding: EdgeInsets.only(top: 16),
          child: Row(
            // mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            // color:Colors.red,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Text('登记日期： 2019-09-02',
                      style: Theme.of(context).textTheme.caption),
                  Padding(
                    padding: EdgeInsets.only(top: 4),
                    child: Text(
                      '登记人: 管理员',
                      style: Theme.of(context).textTheme.caption,
                    ),
                  )
                ],
              )
            ],
          ),
        ),
        state: StepState.complete,
        isActive: true,
      ),
      Step(
          title: Text('已审核'),
          content: Text('已登记啊啊'),
          state: StepState.complete,
          isActive: true,
          subtitle: Text('已审核 小标题')),
      Step(
          title: Text('已完成'),
          content: Text('已完成小标题'),
          state: StepState.disabled,
          isActive: false,
          subtitle: Text('已登记小标题')),
      Step(
          title: Text('验证'),
          content: Text('已登记啊啊'),
          state: StepState.complete,
          isActive: false,
          subtitle: Text('已验证小标题')),
    ];
    return Stepper(
      onStepTapped: (int index) => _onStepTapped(index),
      // type: StepperType.value,
      currentStep: _currentStep,
      steps: steps,
      controlsBuilder: (BuildContext context,
          {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
        return Container();
      },
    );
  }

  Widget _body(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[Flexible(child: _buildItem(context, 1))],
    );

    // return ListView.builder(
    //   itemBuilder :  (context,index) =>_buildItem(context, index),
    //   itemCount: 1,
    // );
  }
}
