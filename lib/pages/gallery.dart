import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:cached_network_image/cached_network_image.dart';

class GalleryPage extends StatelessWidget {
  List<String> urls = [];

  GalleryPage(this.urls);
  @override
  Widget build(BuildContext context) {
    List<PhotoViewGalleryPageOptions> options = urls.map((url) {
      return PhotoViewGalleryPageOptions(
          imageProvider: CachedNetworkImageProvider(url),
          heroTag: "image $url",
        );
    }).toList();

    return Container(
        child: PhotoViewGallery(
      pageOptions: options,
      backgroundDecoration: BoxDecoration(color: Colors.black87),
    ));
  }
}
