import 'package:safety_platform/http.dart';
import 'package:safety_platform/models/message.dart';

class MessageRepository {
  final String url = '/mss/messages/';
  Future<MessagePage> loadMessages({int page: 1, int pageSize: 10}) async {
    final String cancelKey = '_load_messages_key';

    Map<String, String> data = {
      'page': page.toString(),
      'page_size': pageSize.toString()
    };

    var res = await httpGet(url, data: data);

    return MessagePage.fromJson(res);
  }
}
