import 'package:safety_platform/models/attachment.dart';
import 'package:safety_platform/http.dart';
import 'dart:io';
class AttachmentRepository {

  Future<Attachment> storeAttachment(String target, Attachment attachment) async{
    dynamic res =await  httpUpload(File(attachment.localPath), target: target);

    return Attachment.fromJson(res);
  }
}