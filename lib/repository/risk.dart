import 'package:safety_platform/models/risk_unit.dart';
import 'package:safety_platform/http.dart';
import 'package:dio/dio.dart';
class RiskRepository {
   CancelToken _token ; 

  Future<List<RiskUnit>> getAllRiskUnit() async {
    final String url = '/risk_unit/';
    Map<String, String> data = Map();
    data['page_size'] = '1000';
    if (_token != null) {
      _token.cancel();
    }
    _token = CancelToken();
    var res = await httpGet(url, data: data, cache: true,cancelToken:_token);
    _token = null;
    return RiskUnitPage.fromJson(res).results;
  
  
  }

  Future<RiskUnit> getUnitFromCache(String unitId) async {
    return null;
  }

  Future<RiskUnit> getRiskUnit(String unitId) async {
    final String url  = '/risk_unit/$unitId/';
    if (_token != null) {
      _token.cancel();
    }
    _token =CancelToken();
    var res =await httpGet(url,cancelToken:_token);
    _token = null;

    return RiskUnit.fromJson(res);
  }
}
