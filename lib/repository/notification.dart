
import 'package:safety_platform/models/received_notification_page.dart';
import 'package:safety_platform/models/received_notification.dart';
import 'package:safety_platform/http.dart';
class NotificationRepository {

  
  Future<ReceivedNotification> markNotificationSigned(int id) async
  {
    final String url = '/mss/received_notifications/$id/';

    var res = await httpPut(url, data:{
      'signed': true,
    });
    return ReceivedNotification.fromJson(res);
  }
  Future <ReceivedNotification> markNotificationRead(int id) async {
    final String url = '/mss/received_notifications/$id/';

    var res = await httpPut(url, data:{
      'read': true,
    });
    return ReceivedNotification.fromJson(res);

  }
  Future<ReceivedNotificationPage>loadReceivedNotification({int page:1, int pageSize:10}) async {
    final String url = '/mss/received_notifications/';
    Map<String,String>  data = {

      'page':page.toString(),
      'page_size': pageSize.toString(),
    };

    var res  = await httpGet(url,data: data);

    return ReceivedNotificationPage.fromJson(res);
  }
}