import 'dart:ui' show Color;
import 'package:flutter/material.dart'
    show
        TextStyle,
        Colors,
        Widget,
        Text,
        FontWeight,
        EdgeInsets,
        Padding,
        Image,
        BoxFit;

final Color pendigColor = Color.fromARGB(255, 255, 180, 0);
final Color riskyColor = Color.fromARGB(255, 139, 0, 0);
final Color doneColor = Color.fromARGB(255, 30, 144, 255);
final Color secondTextColor = Color.fromARGB(255, 241, 241, 241);
final Color borderColor = Color.fromARGB(255, 241, 241, 241);

final Color lowColor =Color.fromARGB(255, 19, 148, 218);
final Color normalColor =Colors.yellow[600];
final Color highColor = Color.fromARGB(255, 253, 181, 10);

final TextStyle secondTextStyle = TextStyle(
  color: Colors.grey,
);

final Color rightAnswerColor = Color.fromARGB(255, 0, 153, 102);
final Color negtiveAnswerColor = Color.fromARGB(255, 255, 0, 0);
Image attachmentImage(String contentType) {
  String leadingImg;
  switch (contentType) {
    case 'image/png':
      {
        leadingImg = 'assets/png.png';
        break;
      }
    case 'image/jpeg':
    case 'image/jpg':
      {
        leadingImg = 'assets/jpg.png';
        break;
      }
    default:
      {
        leadingImg = 'assets/unknow.png';
        break;
      }
  }
  return Image.asset(leadingImg, fit: BoxFit.fill,width: 24,);
}

Widget backgroundTitle(String title) {
  return Padding(
      child: Text(
        title,
        style: TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
      padding: EdgeInsets.fromLTRB(16, 8, 16, 8));
}
